//
//  UIImage+gradient.swift
//  TelematicsApp
//

import UIKit

class ScaledHeightImageView: UIImageView {

    override var intrinsicContentSize: CGSize {

        if let myImage = self.image {
            let myImageHeight = myImage.size.height
            let myImageWidth = myImage.size.width
            let imageRatio = myImageWidth/myImageHeight
            let myViewWidth = self.frame.size.width
            
            let height = myImageWidth*imageRatio

            return CGSize(width: myViewWidth, height: height)
        }

        return CGSize(width: -1.0, height: -1.0)
    }

}

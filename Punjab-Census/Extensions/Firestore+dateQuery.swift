//
//  Firestore+dateQuery.swift
//  TelematicsApp
//

import Firebase

extension Query {
    func whereField(_ field: String, isSince value: Date) -> Query {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: value)
        guard
            let start = Calendar.current.date(from: components)
        else {
            fatalError("Could not find start date.")
        }
        return whereField(field, isGreaterThan: start)
    }
}

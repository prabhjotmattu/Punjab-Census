//
//  Double.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 5/7/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import Foundation

extension Double {
    static func randomColorRGB() -> Double {
        return Double(arc4random()) / Double(UInt32.max)
    }
}

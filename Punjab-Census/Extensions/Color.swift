//
//  Color.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 5/7/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import SwiftUI

extension Color {
    static func random() -> Color {
        return Color(red: .randomColorRGB(), green: .randomColorRGB(), blue: .randomColorRGB())
    }
}

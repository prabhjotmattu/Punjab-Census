//
//  String+isNumeric.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 4/1/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Foundation

extension String {
    
    func isNumeric() -> Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
    
}

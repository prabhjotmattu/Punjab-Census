//
//  DatabaseManager.swift
//  TelematicsApp
//
//  Created by William Smith on 10/7/19.
//  Copyright © 2019 UTDesign. All rights reserved.
//

import Foundation
import Firebase

/// On application initialization, this class triggers the Firebase caching mechanism and detects errors. The app will terminate at this stage if there are issues.
class CacheHelper {
    
    let completion: (_ status: FirestoreLoadStatus) -> Void
    private var working = false
    private var user: User!
    
    
    /// Creates a new CacheHelper object.
    /// - Parameter completion: The block that triggers on cache update/failure.
    init(completion: @escaping (_ status: FirestoreLoadStatus) -> Void) {
        self.completion = completion
    }
    
    
    /// Updates the caches, notifying the block that was set in initialization on completion.
    func updateCaches() {
        if(working) {
            print("Race condition avoided. Please check code.")
            completion(.asynchRaceCondition)
            return
        }
        working = true
        guard let user = Auth.auth().currentUser else {
            completion(.userNotLoggedIn)
            working = false
            return
        }
        
        
        Firestore.firestore().collection("users").document(user.uid).getDocument(completion: userInformationAvailable(document:error:))
    }
    
    private func userInformationAvailable(document: DocumentSnapshot?, error: Error?) {
        do {
            if let error = error { throw error }
            guard let user = try document?.asUser() else { throw FirestoreError.malformedDocument}
            self.user = user
            print("Cache updated for " + user.email)
        } catch {
            print(error.localizedDescription)
            try? Auth.auth().signOut()
            completion(.corruptDataReceived)
            working = false
            return
        }
        VillageManager.shared.userWasLoggedIn(uid: user.uid)
        FamilyManager.shared.userWasLoggedIn(uid: user.uid)
        PersonManager.shared.userWasLoggedIn(uid: user.uid)
        completion(.ready)
        working = false
    }
}

extension DocumentSnapshot {
    
    /// Attempts to cast a document to the Person model object for use. This object is read-only and can not update Firebase on it's own.
    func asUser() throws -> User {
        let uid = self.documentID
        guard let email = self.get("email") as? String else {
            throw FirestoreError.malformedDocument
        }
        return User(uid: uid, email: email)
    }
    
    func asVillage() throws -> Village {
        let vid = self.documentID
        guard let uid = self.get("uid") as? String,
            let villageName = self.get("villageName") as? String,
            let numOfResident = self.get("numOfResident") as? Int,
            let numOfPubSchool = self.get("numOfPubSchool") as? Int,
            let numOfPrivSchool = self.get("numOfPrivSchool") as? Int,
            let comment = self.get("comment") as? String else {
                throw FirestoreError.malformedDocument
        }
        return Village(uid: uid, vid: vid, villageName: villageName, numOfResident: numOfResident, numOfPubSchool: numOfPubSchool, numOfPrivSchool: numOfPrivSchool, comment: comment)
    }
    
    func asFamily() throws -> Family {
        let fid = self.documentID
        guard let uid = self.get("uid") as? String,
            let familyName = self.get("familyName") as? String,
            let income = self.get("income") as? Double,
            let transportation = self.get("transportation") as? String,
            let tractor = self.get("tractor") as? Bool,
            let overseas = self.get("overseas") as? Bool,
            let police = self.get("police") as? Bool,
            let internet = self.get("internet") as? Bool,
            let electricity = self.get("electricity") as? Bool,
            let healthcare = self.get("healthcare") as? Bool,
            let water = self.get("water") as? Bool,
            let food = self.get("food") as? Bool,
            let comment = self.get("comment") as? String,
            let members = self.get("members") as? [String] else {
                throw FirestoreError.malformedDocument
        }
        return Family(uid: uid, fid: fid, familyName: familyName, income: income, transportation: transportation, tractor: tractor, overseas: overseas, police: police, internet: internet, electricity: electricity, healthcare: healthcare, water: water, food: food, comment: comment, members: members)
    }
    
    func asPerson() throws -> Person {
        let pid = self.documentID
        guard let uid = self.get("uid") as? String,
        let vid = self.get("vid") as? String,
        let fid = self.get("fid") as? String,
        let firstName = self.get("firstName") as? String,
        let lastName = self.get("lastName") as? String,
        let age = self.get("age") as? Int,
        let gender = self.get("gender") as? String,
        let religion = self.get("religion") as? String,
        let children = self.get("children") as? Bool,
        let drug = self.get("drug") as? Bool,
        let school = self.get("school") as? String,
        let education = self.get("education") as? String,
        let profession = self.get("profession") as? String,
        let comment = self.get("comment") as? String else {
            throw FirestoreError.malformedDocument
        }
        return Person(uid: uid, pid: pid, vid: vid, fid: fid, firstName: firstName, lastName: lastName, age: age, gender: gender, religion: religion, children: children, drug: drug, school: school, education: education, profession: profession, comment: comment)
    }
    
}

enum FirestoreError: Error {
    case unknownError
    case userNotLoggedIn
    case invalidCache
    case malformedDocument
}

enum FirestoreLoadStatus {
    case ready
    case userNotLoggedIn
    case asynchRaceCondition
    case corruptDataReceived
}


//
//  DeviceManager.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 6/11/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import Foundation
import Firebase

class VillageManager {
    static let shared = VillageManager()
    private var snapshotListener: ListenerRegistration?
    
    let letters = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z".components(separatedBy: " ")
    var titles: [String] = []
    var filteredVillages: [Village] = []
    var uid: String = ""
    
    private var _allVillages: [Village] = []
    var allVillages: [Village] {
        get {
            return _allVillages
        }
    }
    
    func userWasLoggedIn(uid: String) {
        print("UID: \(self.uid)")
        self.uid = uid
        snapshotListener?.remove()
        snapshotListener = Firestore.firestore().collection("villages").whereField("uid", isEqualTo: uid).addSnapshotListener { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                do {
                    var villages: [Village] = []
                    for village in querySnapshot!.documents {
                        print("Village ID: \(village.documentID)")
                        villages.append(try village.asVillage())
                    }
                    self._allVillages = villages
                    self.getTitles()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadVillageData"), object: nil)
                    //print("All villages: \(self._allVillages)")
                    //print("Titles \(self.titles)")
                } catch {
                    print("Error parsing villages: \(error)")
                    return
                }
            }
        }
    }
    
    func userWasLoggedOut() {
        snapshotListener?.remove()
        _allVillages = []
    }
    
    func getTitles() {
        titles.removeAll()
        for village in allVillages {
            if self.letters.contains(village.villageName.first?.uppercased() ?? "") && !self.titles.contains(village.villageName.first?.uppercased() ?? "") {
                self.titles.append(String(village.villageName.first?.uppercased() ?? ""))
            }
        }
        titles.sort()
        //print("Get titles: \(titles)")
    }


}

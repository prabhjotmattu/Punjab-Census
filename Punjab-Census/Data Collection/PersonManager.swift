//
//  PersonManager.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 6/12/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import Foundation
import Firebase

class PersonManager {
    static let shared = PersonManager()
    private var snapshotListener: ListenerRegistration?
    
    let letters = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z".components(separatedBy: " ")
    var titles: [String] = []
    var filteredPeople: [Person] = []
    var uid: String = ""
    
    private var _allPeople: [Person] = []
    var allPeople: [Person] {
        get {
            return _allPeople
        }
    }
    
    func userWasLoggedIn(uid: String) {
        self.uid = uid
        snapshotListener?.remove()
        snapshotListener = Firestore.firestore().collection("people").whereField("uid", isEqualTo: uid).addSnapshotListener { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                do {
                    var people: [Person] = []
                    for person in querySnapshot!.documents {
                        //print("Village ID: \(person.documentID)")
                        people.append(try person.asPerson())
                    }
                    self._allPeople = people
                    self.getTitles()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadPersonData"), object: nil)
                    //print(self._allPeople)
                } catch {
                    print("Error parsing people: \(error)")
                    return
                }
            }
        }
    }
    
    func userWasLoggedOut() {
        snapshotListener?.remove()
        _allPeople = []
    }
    
    func getTitles() {
        titles.removeAll()
        for person in allPeople {
            if self.letters.contains(person.firstName.first?.uppercased() ?? "") && !self.titles.contains(person.firstName.first?.uppercased() ?? "") {
                self.titles.append(String(person.firstName.first?.uppercased() ?? ""))
            }
        }
        self.titles.sort()
    }


}

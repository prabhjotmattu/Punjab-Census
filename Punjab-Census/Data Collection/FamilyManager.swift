//
//  FamilyManager.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 6/12/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import Foundation
import Firebase

class FamilyManager {
    static let shared = FamilyManager()
    private var snapshotListener: ListenerRegistration?
    
    let letters = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z".components(separatedBy: " ")
    var titles: [String] = []
    var filteredFamilies: [Family] = []
    var uid: String = ""
    
    private var _allFamilies: [Family] = []
    var allFamilies: [Family] {
        get {
            return _allFamilies
        }
    }
    
    func userWasLoggedIn(uid: String) {
        self.uid = uid
        snapshotListener?.remove()
        snapshotListener = Firestore.firestore().collection("families").whereField("uid", isEqualTo: uid).addSnapshotListener { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                do {
                    var families: [Family] = []
                    for family in querySnapshot!.documents {
                        //print("Family ID: \(family.documentID)")
                        families.append(try family.asFamily())
                    }
                    self._allFamilies = families
                    self.getTitles()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadFamilyData"), object: nil)
                    //print(self._allFamilies)
                } catch {
                    print("Error parsing families: \(error)")
                    return
                }
            }
        }
    }
    
    func userWasLoggedOut() {
        snapshotListener?.remove()
        _allFamilies = []
    }
    
    func getTitles() {
        titles.removeAll()
        for family in allFamilies {
            if self.letters.contains(family.familyName.first?.uppercased() ?? "") && !self.titles.contains(family.familyName.first?.uppercased() ?? "") {
                self.titles.append(String(family.familyName.first?.uppercased() ?? ""))
            }
        }
        self.titles.sort()
    }
}

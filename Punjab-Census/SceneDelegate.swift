//
//  SceneDelegate.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 3/21/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import SwiftUI
import Firebase

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    private var _allPeople: [Person] = []
    var allPeople: [Person] { get {return _allPeople}}
    
    private var _allFamilies: [Family] = []
    var allFamilies: [Family] { get {return _allFamilies}}
    
    private var _allVillages: [Village] = []
    var allVillages: [Village] { get {return _allVillages}}
    
    let userDefault = UserDefaults.standard
    let launchedBefore = UserDefaults.standard.bool(forKey: "usersignedin")
    
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let _ = (scene as? UIWindowScene) else { return }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        if userDefault.bool(forKey: "usersignedin") {
            let mainTabBarController = storyboard.instantiateViewController(identifier: "root")
            window?.rootViewController = mainTabBarController
            let uid = Auth.auth().currentUser?.uid
            VillageManager.shared.userWasLoggedIn(uid: uid ?? "")
            FamilyManager.shared.userWasLoggedIn(uid: uid ?? "")
            PersonManager.shared.userWasLoggedIn(uid: uid ?? "")
            setControllers()
        
        } else {
            let loginController = storyboard.instantiateViewController(identifier: "login")
            window?.rootViewController = loginController
        }
        
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    func changeRootViewController(_ vc: UIViewController, _ type: String, animated: Bool = true) {
        guard let window = self.window else {
            return
        }
        
        window.rootViewController = vc
        
        if type == "root" {
            
            setControllers()
            /*
             let firstVillage = villagesMasterViewController.allVillages.first
             villagesDetailViewController.village = firstVillage
             villagesMasterViewController.delegate = villagesDetailViewController
             */
            
        }
        
        
        
        UIView.transition(with: window, duration: 0.5, options: [.transitionFlipFromLeft], animations: nil, completion: nil)
    }
    
    func setControllers() {
        guard let tabBarController = window?.rootViewController as? UITabBarController else {
            fatalError()
        }
        
        guard
            let peopleSplitViewController = tabBarController.viewControllers?[2] as? UISplitViewController,
            let peopleLeftNavController = peopleSplitViewController.viewControllers.first as? UINavigationController,
            let peopleRightNavController = peopleSplitViewController.viewControllers.last as? UINavigationController,
            let peopleMasterViewController = peopleLeftNavController.viewControllers.first as? PeopleMasterViewController,
            let peopleDetailViewController = peopleRightNavController.viewControllers.last as? PeopleDetailViewController
            else {
                fatalError()
        }
        peopleMasterViewController.delegate = peopleDetailViewController
        /*
         let firstPerson = peopleMasterViewController.allPeople.first
         peopleDetailViewController.person = firstPerson
         peopleMasterViewController.delegate = peopleDetailViewController
         */
        // Get view controllers for Families tab
        guard
            let familiesSplitViewController = tabBarController.viewControllers?[1] as? UISplitViewController,
            let familiesLeftNavController = familiesSplitViewController.viewControllers.first as? UINavigationController,
            let familiesRightNavController = familiesSplitViewController.viewControllers.last as? UINavigationController,
            let familiesMasterViewController = familiesLeftNavController.viewControllers.first as? FamiliesMasterViewController,
            let familiesDetailViewController = familiesRightNavController.viewControllers.last as? FamiliesDetailViewController
            else {
                fatalError()
        }
        familiesMasterViewController.delegate = familiesDetailViewController
        /*
         let firstFamiliy = familiesMasterViewController.allFamilies.first
         familiesDetailViewController.family = firstFamiliy
         familiesMasterViewController.delegate = familiesDetailViewController
         */
        
        // Get view controllers for Villages tab
        guard
            let villagesSplitViewController = tabBarController.viewControllers?[0] as? UISplitViewController,
            let villagesLeftNavController = villagesSplitViewController.viewControllers.first as? UINavigationController,
            let villagesRightNavController = villagesSplitViewController.viewControllers.last as? UINavigationController,
            let villagesMasterViewController = villagesLeftNavController.viewControllers.first as? VillagesMasterViewController,
            let villagesDetailViewController = villagesRightNavController.viewControllers.last as? VillagesDetailViewController
            else {
                fatalError()
        }
        villagesMasterViewController.delegate = villagesDetailViewController
    }
    
    
}


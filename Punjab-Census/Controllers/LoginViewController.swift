//
//  LoginViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 3/21/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var eyeButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    var passwordSecure = true
    
    let loadingAlert = UIAlertController(title: nil, message: "Logging in...", preferredStyle: .alert)
    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
    
    let userDefault = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        passwordSecure = passwordText.isSecureTextEntry
        loadingAlert.view.addSubview(loadingIndicator)
        loadingIndicator.startAnimating()

        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        usernameText.setBottomBorder()
        passwordText.setBottomBorder()
        
        loginButton.layer.cornerRadius = 25
        loginButton.layer.borderWidth = 1
        loginButton.layer.borderColor = UIColor.clear.cgColor
        loginButton.contentEdgeInsets = UIEdgeInsets(top: loginButton.contentEdgeInsets.top + 15, left: loginButton.contentEdgeInsets.left + 15, bottom: loginButton.contentEdgeInsets.bottom + 15, right: loginButton.contentEdgeInsets.right + 15)
    }
    
    @IBAction func togglePassword(_ sender: UIButton) {
        passwordSecure = !passwordSecure
        passwordText.isSecureTextEntry = passwordSecure
        
        if passwordSecure {
            eyeButton.setImage(UIImage(systemName: "eye.fill"), for: .normal)
        } else {
            eyeButton.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
        }
    }
    
    @IBAction func signIn(_ sender: UIButton) {
        present(loadingAlert, animated: true, completion: {
            Auth.auth().signIn(withEmail: self.usernameText.text ?? "", password: self.passwordText.text ?? "") { [weak self] user,
                error in
                guard let self = self else {return}
                if let error = error {
                    print("Authentication Error: \(error.localizedDescription)")
                    self.showAuthError()
                } else if user != nil {
                    print("Found user")
                    self.userDefault.set(true, forKey: "usersignedin")
                    self.userDefault.synchronize()
                    let cacheHelper = CacheHelper(completion: self.cache(status:))
                    cacheHelper.updateCaches()
                }
                
            }
        })
    }

    func cache(status: FirestoreLoadStatus) {
        switch status {
        case .ready:
            print("Ready to login")
            sendToRoot()
        case .userNotLoggedIn:
            showAuthError()
        case .asynchRaceCondition:
            showErrorMessage(title: "A fatal error occurred", message: "Too many calls to cacheHelper.")
        case .corruptDataReceived:
            showErrorMessage(title: "A fatal error occurred", message: "Application recieved malformed data from server. Please check Firebase console and restart application.")
        }
    }
    
    func sendToRoot() {
        print("Send to root")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabBarController = storyboard.instantiateViewController(identifier: "root")
        
        
        //UserDefaults.standard.set(true, forKey: "status")
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(tabBarController, "root")
        
        
    }
    
    func showErrorMessage(title: String?, message: String?) {
        loadingIndicator.removeFromSuperview()
        loadingAlert.title = title
        loadingAlert.message = message
    }
    
    func showAuthError() {
        loadingAlert.dismiss(animated: true, completion: {
            let alert = UIAlertController(title: "Invalid Credentials", message: "Please check your input and try again", preferredStyle: .alert)
            let dismiss = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(dismiss)
            self.present(alert, animated: true, completion: nil)
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

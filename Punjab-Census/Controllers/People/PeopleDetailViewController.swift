//
//  PeopleDetailViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 3/22/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

class PeopleDetailViewController: UITableViewController {
    
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var religionLabel: UILabel!
    @IBOutlet weak var schoolLabel: UILabel!
    @IBOutlet weak var educationLabel: UILabel!
    @IBOutlet weak var professionLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var childLabel: UILabel!
    @IBOutlet weak var drugLabel: UILabel!
    @IBOutlet weak var villageLabel: UILabel!
    @IBOutlet weak var familyLabel: UILabel!
    
    @IBOutlet weak var editButton: UIBarButtonItem!
    var initialLoad = false
    
    var fid: String?
    var vid: String?
    
    var person: Person? {
        didSet {
            refreshUI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editButton.isEnabled = false
    }
    
    private func refreshUI() {
        loadViewIfNeeded()
        
        self.title = "\(person?.firstName ?? "") \(person?.lastName ?? "")"
        ageLabel.text = "\(person?.age ?? 0)"
        genderLabel.text = person?.gender
        religionLabel.text = person?.religion
        schoolLabel.text = person?.school
        educationLabel.text = person?.education
        professionLabel.text = person?.profession
        commentLabel.text = person?.comment
        fid = person?.fid
        vid = person?.vid
        
        switch person?.children {
        case true:
            childLabel.text = "Yes"
        case false:
            childLabel.text = "No"
        default:
            childLabel.text = "Not Found"
        }
        
        switch person?.drug {
        case true:
            drugLabel.text = "Yes"
        case false:
            drugLabel.text = "No"
        default:
            drugLabel.text = "Not Found"
        }
        
        if let fids = person?.fid {
            getFamily(fid: fids)
        }
        
        if let vids = person?.vid {
            getVillage(vid: vids)
        }
    }
    
    
    func getFamily(fid: String) {
        let ref = Firestore.firestore().collection("families").document(fid)
        ref.getDocument { (document, error) in
            if let document = document, document.exists {
                let familyName = document.get("familyName")
                self.familyLabel.text = "\(familyName!)"
            } else {
                print("Document does not exist")
            }
        }
    }
    
    func getVillage(vid: String) {
        print("Get Village Called")
        let ref = Firestore.firestore().collection("villages").document(vid)
        ref.getDocument { (document, error) in
            if let document = document, document.exists {
                let villageName = document.get("villageName")
                self.villageLabel.text = "\(villageName!)"
                print("Village Name: \(villageName!)")
            } else {
                print("Document does not exist")
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !initialLoad {
            return 0.0
        } else {
            return 50.0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if !initialLoad {
            return ""
        } else {
            switch section {
            case 0:
                return "Personal Info"
            case 1:
                return "Education/Work"
            case 2:
                return "Comments"
            default:
                return ""
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editPersonSegue" {
            let controller = segue.destination as! EditPersonViewController
            controller.person = person
        }
    }
}

extension PeopleDetailViewController: PersonSelectionDelegate {
    func personSelected(_ newPerson: Person) {
        person = newPerson
        if !initialLoad {
            initialLoad = true
            tableView.reloadData()
            editButton.isEnabled = true
        }
    }
}

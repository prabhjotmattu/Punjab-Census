//
//  FamilySelectViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 4/8/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

protocol FamilySelectViewControllerDelegate: class {
    func familySelectViewController(_ controller: FamilySelectViewController, didFinishSelecting family: String, _ fid: String)
}

class FamilySelectViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    //private var familySnapshotListener: ListenerRegistration?
    var previousFamily: String?
    var familyDictionary: [String:String] = [:]

    @IBOutlet weak var familyPicker: UIPickerView!
    var pickerData: [String] = []
    var unsortedData: [String] = []
    weak var delegate: FamilySelectViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.familyPicker.delegate = self
        self.familyPicker.delegate = self
        getFamilies()
    }
    
    @IBAction func done() {
        let selectedRow = familyPicker.selectedRow(inComponent: 0)
        let familyName = pickerData[selectedRow]
        if let fid = familyDictionary[familyName] {
            delegate?.familySelectViewController(self, didFinishSelecting: familyName, fid)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    private func getFamilies() {
        /*
        familySnapshotListener?.remove()
        familySnapshotListener = Firestore.firestore().collection("families").order(by: FieldPath(["familyName"]), descending: true).addSnapshotListener {
            (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                do {
                    for family in querySnapshot!.documents {
                        let fid = family.documentID
                        let familyName = family.get("familyName") as? String ?? "not found"
                        self.familyDictionary[familyName] = fid
                    }
                    self.pickerData = Array(self.familyDictionary.keys)
                    //self.unsortedData = self.pickerData
                    self.pickerData.sort()
                    //print(self.pickerData)
                }
            }
            
        }
        */
        for family in FamilyManager.shared.allFamilies {
            familyDictionary[family.familyName] = family.fid
        }
        pickerData = Array(self.familyDictionary.keys)
        pickerData.sort()
        familyPicker.reloadAllComponents()
        if let index = pickerData.firstIndex(where: { $0 == previousFamily}) {
            familyPicker.selectRow(index, inComponent: 0, animated: true)
        }
    }
}

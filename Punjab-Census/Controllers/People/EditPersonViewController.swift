//
//  EditPersonViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 4/25/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

class EditPersonViewController: UITableViewController, FamilySelectViewControllerDelegate, VillageSelectViewControllerDelegate {
    
    func familySelectViewController(_ controller: FamilySelectViewController, didFinishSelecting family: String, _ fid: String) {
        familyLabel.text = family
        familyLabel.textColor = UIColor.label
        familyLabel.font = UIFont.boldSystemFont(ofSize: familyLabel.font.pointSize)
        if let index = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: index, animated: true)
        }
        familySelected = true
        self.newFid = fid
        famSelected = true
        checkRequired()
        dismiss(animated: true, completion: nil)
    }
    
    func villageSelectViewController(_ controller: VillageSelectViewController, didFinishSelecting village: String, _ vid: String) {
        villageLabel.text = village
        villageLabel.textColor = UIColor.label
        villageLabel.font = UIFont.boldSystemFont(ofSize: villageLabel.font.pointSize)
        if let index = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: index, animated: true)
        }
        villageSelected = true
        self.newVid = vid
        vilSelected = true
        checkRequired()
        dismiss(animated: true, completion: nil)
    }
    
    
    var person: Person?

    @IBOutlet weak var firstNameText: UITextField!
    @IBOutlet weak var lastNameText: UITextField!
    @IBOutlet weak var ageText: UITextField!
    @IBOutlet weak var religionText: UITextField!
    @IBOutlet weak var schoolText: UITextField!
    @IBOutlet weak var professionText: UITextField!
    @IBOutlet weak var commentText: UITextField!
    
    @IBOutlet weak var familyLabel: UILabel!
    @IBOutlet weak var villageLabel: UILabel!
    
    @IBOutlet weak var genderControl: UISegmentedControl!
    @IBOutlet weak var childrenControl: UISegmentedControl!
    @IBOutlet weak var drugControl: UISegmentedControl!
    @IBOutlet weak var educationControl: UISegmentedControl!
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var gender = "N/A"
    var child = false
    var drug = false
    var education = "N/A"
    var familySelected = true
    var villageSelected = true
    var pid: String?
    var famSelected = false
    var vilSelected = false
    var oldFid: String?
    var oldVid: String?
    var newFid: String?
    var newVid: String?
    var fid: String?
    var vid: String?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pid = person?.pid
        oldFid = person?.fid
        oldVid = person?.vid
        firstNameText.text = person?.firstName
        lastNameText.text = person?.lastName
        ageText.text = String(person?.age ?? 0)
        religionText.text = person?.religion
        schoolText.text = person?.school
        professionText.text = person?.profession
        commentText.text = person?.comment
        
        getFamily(fid: oldFid!)
        getVillage(vid: oldVid!)
        
        //familyLabel.text = person?.family
        //villageLabel.text = person?.village
        
        print("Gender: \(person!.gender)")
        
        if person!.gender == "Male" {
            genderControl.selectedSegmentIndex = 0
        } else if person!.gender == "Female" {
            genderControl.selectedSegmentIndex = 1
        }
        
        if person!.children {
            childrenControl.selectedSegmentIndex = 0
        } else {
            childrenControl.selectedSegmentIndex = 1
        }
        
        if person!.drug {
            drugControl.selectedSegmentIndex = 0
        } else {
            drugControl.selectedSegmentIndex = 1
        }
        
        print("Education: \(person!.education)")
        
        if person!.education == "Attending School" {
            educationControl.selectedSegmentIndex = 0
        } else if person!.education == "High School" {
            educationControl.selectedSegmentIndex = 1
        } else if person!.education == "Bachelors" {
            educationControl.selectedSegmentIndex = 3
        } else if person!.education == "Masters" {
            educationControl.selectedSegmentIndex = 4
        }
        
    }
    
    // MARK: - Segmented Control Functions
    
    @IBAction func switchIndexChanged(_ control: UISegmentedControl) {
        switch control.tag {
        case 1:
            switch control.selectedSegmentIndex {
            case 0:
                gender = "Male"
            case 1:
                gender = "Female"
            default:
                break
            }
            //print("Gender: \(gender)")
        case 2:
            switch control.selectedSegmentIndex {
            case 0:
                child = true
            case 1:
                child = false
            default:
                child = false
            }
        case 3:
            switch control.selectedSegmentIndex {
            case 0:
                drug = true
            case 1:
                drug = false
            default:
                drug = false
            }
        case 4:
            switch control.selectedSegmentIndex {
            case 0:
                education = "Attending School"
            case 1:
                education = "High School"
            case 2:
                education = "Bachelors"
            case 3:
                education = "Masters"
            case 4:
                education = "N/A"
            default:
                education = "N/A"
            }
            //print("Education: \(education)")
        default:
            break
        }
        checkRequired()
    }
    
    // MARK: - Input Functions
    
    @IBAction func textChanged(_ textField: UITextField) {
        if(textField.tag == 1) {
            if let text = textField.text, let isNum = textField.text?.isNumeric(), !isNum {
                textField.text = String(text.dropLast())
            }
        }
        checkRequired()
    }
    
    func checkRequired() {
        if(firstNameText.hasText && lastNameText.hasText && ageText.hasText && genderControl.selectedSegmentIndex != -1 && childrenControl.selectedSegmentIndex != -1 && drugControl.selectedSegmentIndex != -1 && educationControl.selectedSegmentIndex != -1 && familySelected == true && villageSelected == true) {
            doneButton.isEnabled = true
        } else {
            doneButton.isEnabled = false
        }
    }
    
    // MARK: - Navigation Bar Button Functions
    
    @IBAction func cancel(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
  
    
    @IBAction func done(_ sender: Any) {
        if (!religionText.hasText) {
            religionText.text = "N/A"
        }
        if (!schoolText.hasText) {
            schoolText.text = "N/A"
        }
        if (!professionText.hasText) {
            professionText.text = "N/A"
        }
        
        if famSelected {
            fid = newFid
        } else {
            fid = oldFid
        }
        
        if vilSelected {
            vid = newVid
        } else {
            vid = oldVid
        }
        
        //let familyName = familyLabel.text ?? "N/A"
        //let villageName = villageLabel.text ?? "N/A"
        let firstName = firstNameText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "N/A"
        let lastName = lastNameText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "N/A"
        let age = Int(ageText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "0") ?? 0
        let religion = religionText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "N/A"
        let school = schoolText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "N/A"
        let profession = professionText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "N/A"
        let comment = commentText.text ?? ""
        
        //print("Gender: \(gender)")
        //print("Education: \(education)")
        
        var personRef: DocumentReference? = nil
        personRef = Firestore.firestore().collection("people").document(pid!)
        personRef!.updateData([
            "vid" : vid ?? "N/A",
            "fid" : fid ?? "N/A",
            "firstName" : firstName,
            "lastName" : lastName,
            "age" : age,
            "religion" : religion,
            "school" : school,
            "education" : education,
            "profession" : profession,
            "comment" : comment,
            "gender" : gender,
            "children" : child,
            "drug" : drug]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    if self.famSelected {
                        self.updateFamily(pid: personRef!.documentID)
                    }
                    print("Document successfully updated")
                }
        }
        navigationController?.popViewController(animated: true)
    }
    
    func updateFamily(pid: String) {
        var familyRef: DocumentReference? = nil
        familyRef = Firestore.firestore().collection("families").document(fid!)
        familyRef!.updateData([
            "members": FieldValue.arrayUnion([pid])]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    self.removeMember(pid: pid)
                    print("Document successfully updated")
                }
        }
    }
    
    func removeMember(pid: String) {
        var familyRef: DocumentReference? = nil
        familyRef = Firestore.firestore().collection("families").document(oldFid!)
        familyRef!.updateData([
            "members": FieldValue.arrayRemove([pid])]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Document successfully updated")
                }
        }
    }
    
    func getFamily(fid: String) {
        let ref = Firestore.firestore().collection("families").document(fid)
        ref.getDocument { (document, error) in
            if let document = document, document.exists {
                let familyName = document.get("familyName")
                self.familyLabel.text = "\(familyName!)"
            } else {
                print("Document does not exist")
            }
        }
    }
    
    func getVillage(vid: String) {
        let ref = Firestore.firestore().collection("villages").document(vid)
        ref.getDocument { (document, error) in
            if let document = document, document.exists {
                let villageName = document.get("villageName")
                self.villageLabel.text = "\(villageName!)"
            } else {
                print("Document does not exist")
            }
        }
    }
 
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "villageSegue" {
            let nav = segue.destination as! UINavigationController
            let controller = nav.topViewController as! VillageSelectViewController
            controller.delegate = self
            controller.previousVillage = villageLabel.text
        } else if segue.identifier == "familySegue" {
            let nav = segue.destination as! UINavigationController
            let controller = nav.topViewController as! FamilySelectViewController
            controller.delegate = self
            controller.previousFamily = familyLabel.text
        }
    }
}

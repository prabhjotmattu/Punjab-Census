//
//  PeopleMasterViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 3/22/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

protocol PersonSelectionDelegate: class {
    func personSelected(_ newPerson: Person)
}

class PeopleMasterViewController: UITableViewController {
    
    weak var delegate: PersonSelectionDelegate?
    let searchController = UISearchController(searchResultsController: nil)
    
    
    var isSearchBarEmpty: Bool { return searchController.searchBar.text?.isEmpty ?? true}
    var isFiltering: Bool { return searchController.isActive && !isSearchBarEmpty}
    
    var lastSelectedRow: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refresh), name: NSNotification.Name(rawValue: "reloadPersonData"), object: nil)
        
        tableView.remembersLastFocusedIndexPath = true
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search People"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        searchController.view.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        PersonManager.shared.getTitles()
        tableView.reloadData()
        searchController.view.layoutIfNeeded()
        if(lastSelectedRow != nil) {
            let selectedCell = tableView.cellForRow(at: lastSelectedRow!)
            selectedCell?.isHighlighted = true
        }
    }
    
    @objc func refresh() {
        self.tableView.reloadData()
        setPersonDetailAfterEdit()
    }
    
    func setPersonDetailAfterEdit() {
        if lastSelectedRow != nil {
            if isFiltering {
                if !PersonManager.shared.filteredPeople.isEmpty {
                    let selectedPerson = PersonManager.shared.filteredPeople[lastSelectedRow!.row]
                    delegate?.personSelected(selectedPerson)
                }
                
            } else {
                var sectionPeople: [Person] = []
                for person in PersonManager.shared.allPeople {
                    if PersonManager.shared.titles[lastSelectedRow!.section] == String(person.firstName.first?.uppercased() ?? "") {
                        sectionPeople.append(person)
                    }
                }
                if !sectionPeople.isEmpty {
                    let selectedPerson = sectionPeople[lastSelectedRow!.row]
                    delegate?.personSelected(selectedPerson)
                }
            }
            tableView.selectRow(at: lastSelectedRow, animated: true, scrollPosition: .none)
        }
        
    }
    
    // MARK: - People Functions
    
    func filterContentForSearchText(_ searchText: String) {
        PersonManager.shared.filteredPeople = PersonManager.shared.allPeople.filter { (person: Person) -> Bool in
            //print("here 2: \(PersonManager.shared.filteredPeople)")
            return person.firstName.lowercased().contains(searchText.lowercased())
        }
        tableView.reloadData()
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return PersonManager.shared.filteredPeople.count
        } else {
            var rows = 0
            for person in PersonManager.shared.allPeople {
                if PersonManager.shared.titles[section] == String(person.firstName.first?.uppercased() ?? "") {
                    rows += 1
                }
            }
            return rows
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isFiltering {
            return ""
        } else {
            return PersonManager.shared.titles[section]
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath)
        
        let firstNameLabel = cell.viewWithTag(1) as! UILabel
        let lastNameLabel = cell.viewWithTag(2) as! UILabel
        
        if isFiltering {
            firstNameLabel.text = PersonManager.shared.filteredPeople[indexPath.row].firstName
            lastNameLabel.text = PersonManager.shared.filteredPeople[indexPath.row].lastName
            return cell
        } else {
            var sectionPeople: [Person] = []
            for person in PersonManager.shared.allPeople {
                if PersonManager.shared.titles[indexPath.section] == String(person.firstName.first?.uppercased() ?? "") {
                    sectionPeople.append(person)
                }
            }
            firstNameLabel.text = sectionPeople[indexPath.row].firstName
            lastNameLabel.text = sectionPeople[indexPath.row].lastName
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(lastSelectedRow != nil) {
            let selectedCell = tableView.cellForRow(at: lastSelectedRow!)
            selectedCell?.isHighlighted = false
        }
        lastSelectedRow = tableView.indexPathForSelectedRow
        
        if isFiltering {
            let selectedPerson = PersonManager.shared.filteredPeople[indexPath.row]
            delegate?.personSelected(selectedPerson)
        } else {
            var sectionPeople: [Person] = []
            for person in PersonManager.shared.allPeople {
                if PersonManager.shared.titles[indexPath.section] == String(person.firstName.first?.uppercased() ?? "") {
                    sectionPeople.append(person)
                }
            }
            let selectedPerson = sectionPeople[indexPath.row]
            delegate?.personSelected(selectedPerson)
        }
    }
 
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            if isFiltering {
                let person = PersonManager.shared.filteredPeople[indexPath.row]
                let pid = person.pid
                let fid = person.fid
                PersonManager.shared.filteredPeople.remove(at: indexPath.row)
                UIView.transition(with: tableView, duration: 1.0, options: .curveEaseInOut, animations: {self.tableView.reloadData()}, completion: nil)
                let db = Firestore.firestore()
                db.collection("people").document(pid).delete() { err in
                    if let err = err {
                        print("Error removing document: \(err)")
                    } else {
                        print("Document successfully removed!")
                    }
                }
                db.collection("families").document(fid).updateData([
                    "members": FieldValue.arrayRemove([pid])]) { err in
                        if let err = err {
                            print("Error adding document: \(err)")
                        } else {
                            print("Document successully updated")
                        }
                }
            } else {
                var sectionPeople: [Person] = []
                for person in PersonManager.shared.allPeople {
                    if PersonManager.shared.titles[indexPath.section] == String(person.firstName.first?.uppercased() ?? "") {
                        sectionPeople.append(person)
                    }
                }
                let pid = sectionPeople[indexPath.row].pid
                let fid = sectionPeople[indexPath.row].fid
                let db = Firestore.firestore()
                db.collection("people").document(pid).delete() { err in
                    if let err = err {
                        print("Error removing document: \(err)")
                    } else {
                        print("Document successfully removed!")
                    }
                }
                db.collection("families").document(fid).updateData([
                    "members": FieldValue.arrayRemove([pid])]) { err in
                        if let err = err {
                            print("Error adding document: \(err)")
                        } else {
                            print("Document successully updated")
                        }
                }
            }
        }
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if isFiltering {
            return 1
        } else {
            return PersonManager.shared.titles.count
        }
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if isFiltering {
            return [""]
        } else {
            return PersonManager.shared.titles
        }
    }
}

extension PeopleMasterViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}

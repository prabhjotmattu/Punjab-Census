//
//  VillageSelectViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 4/7/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

protocol VillageSelectViewControllerDelegate: class {
    func villageSelectViewController(_ controller: VillageSelectViewController, didFinishSelecting village: String, _ vid: String)
}

class VillageSelectViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    //private var villageSnapshotListener: ListenerRegistration?
    var previousVillage: String?
    var villageDictionary: [String:String] = [:]
    
    @IBOutlet weak var villagePicker: UIPickerView!
    var pickerData: [String] = []
    weak var delegate: VillageSelectViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.villagePicker.delegate = self
        self.villagePicker.dataSource = self
        getVillages()
    }
    
    @IBAction func done() {
        let selectedRow = villagePicker.selectedRow(inComponent: 0)
        let villageName = pickerData[selectedRow]
        if let vid = villageDictionary[villageName] {
            delegate?.villageSelectViewController(self, didFinishSelecting: villageName, vid)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    private func getVillages() {
        /*
        villageSnapshotListener?.remove()
        villageSnapshotListener = Firestore.firestore().collection("villages").order(by: FieldPath(["villageName"]), descending: true).addSnapshotListener {
            (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                do {
                    for village in querySnapshot!.documents {
                        let vid = village.documentID
                        let villageName = village.get("villageName") as? String ?? "not found"
                        self.villageDictionary[vid] = villageName
                    }
                    self.pickerData = Array(self.villageDictionary.values)
                    self.pickerData.sort()
                    //print(self.pickerData)
                }
            }
            self.villagePicker.reloadAllComponents()
            if let index = self.pickerData.firstIndex(where: { $0 == self.previousVillage}) {
                self.villagePicker.selectRow(index, inComponent: 0, animated: true)
            }
        }
        */
        for village in VillageManager.shared.allVillages {
            villageDictionary[village.villageName] = village.vid
        }
        pickerData = Array(villageDictionary.keys)
        pickerData.sort()
        villagePicker.reloadAllComponents()
        if let index = pickerData.firstIndex(where: { $0 == previousVillage}) {
            villagePicker.selectRow(index, inComponent: 0, animated: true)
        }
    }
}

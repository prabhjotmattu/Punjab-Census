//
//  NewPersonViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 4/1/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

class NewPersonViewController: UITableViewController, UITextFieldDelegate, VillageSelectViewControllerDelegate, FamilySelectViewControllerDelegate {
    
    func familySelectViewController(_ controller: FamilySelectViewController, didFinishSelecting family: String, _ fid: String) {
        familyLabel.text = family
        familyLabel.textColor = UIColor.label
        familyLabel.font = UIFont.boldSystemFont(ofSize: familyLabel.font.pointSize)
        if let index = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: index, animated: true)
        }
        familySelected = true
        self.fid = fid
        checkRequired()
        dismiss(animated: true, completion: nil)
    }
    
    
    func villageSelectViewController(_ controller: VillageSelectViewController, didFinishSelecting village: String, _ vid: String) {
        villageLabel.text = village
        villageLabel.textColor = UIColor.label
        villageLabel.font = UIFont.boldSystemFont(ofSize: villageLabel.font.pointSize)
        if let index = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: index, animated: true)
        }
        villageSelected = true
        self.vid = vid
        checkRequired()
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var firstNameText: UITextField!
    @IBOutlet weak var lastNameText: UITextField!
    @IBOutlet weak var ageText: UITextField!
    @IBOutlet weak var religionText: UITextField!
    @IBOutlet weak var schoolText: UITextField!
    @IBOutlet weak var professionText: UITextField!
    @IBOutlet weak var commentText: UITextField!
    
    @IBOutlet weak var familyLabel: UILabel!
    @IBOutlet weak var villageLabel: UILabel!
    
    var gender = "N/A"
    var child = false
    var drug = false
    var education = "N/A"
    var familySelected = false
    var fid: String?
    var villageSelected = false
    var vid: String?
    
    
    @IBOutlet weak var genderControl: UISegmentedControl!
    @IBOutlet weak var childrenControl: UISegmentedControl!
    @IBOutlet weak var drugControl: UISegmentedControl!
    @IBOutlet weak var educationControl: UISegmentedControl!
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Segmented Control Functions
    
    @IBAction func switchIndexChanged(_ control: UISegmentedControl) {
        switch control.tag {
        case 1:
            switch control.selectedSegmentIndex {
            case 0:
                gender = "Male"
            case 1:
                gender = "Female"
            default:
                gender = "N/A"
            }
        case 2:
            switch control.selectedSegmentIndex {
            case 0:
                child = true
            case 1:
                child = false
            default:
                child = false
            }
        case 3:
            switch control.selectedSegmentIndex {
            case 0:
                drug = true
            case 1:
                drug = false
            default:
                drug = false
            }
        case 4:
            switch control.selectedSegmentIndex {
            case 0:
                education = "Attending School"
            case 1:
                education = "High School"
            case 2:
                education = "Bachelors"
            case 3:
                education = "Masters"
            case 4:
                education = "N/A"
            default:
                education = "N/A"
            }
        default:
            break
        }
        checkRequired()
    }
    
    
    // MARK: - Input Functions
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        doneButton.isEnabled = false
        return true
    }
    
    @IBAction func textChanged(_ textField: UITextField) {
        if(textField.tag == 1) {
            if let text = textField.text, let isNum = textField.text?.isNumeric(), !isNum {
                textField.text = String(text.dropLast())
            }
        }
        checkRequired()
    }
    
    func checkRequired() {
        if(firstNameText.hasText && lastNameText.hasText && ageText.hasText && genderControl.selectedSegmentIndex != -1 && childrenControl.selectedSegmentIndex != -1 && drugControl.selectedSegmentIndex != -1 && educationControl.selectedSegmentIndex != -1 && familySelected == true && villageSelected == true) {
            doneButton.isEnabled = true
        } else {
            doneButton.isEnabled = false
        }    }
    
    // MARK: - Navigation Bar Button Functions
    
    @IBAction func cancel() {
        dismiss(animated: true, completion: nil)
    }  
    
    @IBAction func done() {
        if (!religionText.hasText) {
            religionText.text = "N/A"
        }
        if (!schoolText.hasText) {
            schoolText.text = "N/A"
        }
        if (!professionText.hasText) {
            professionText.text = "N/A"
        }
        
        //let familyName = familyLabel.text ?? "N/A"
        //let villageName = villageLabel.text ?? "N/A"
        let firstName = firstNameText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "N/A"
        let lastName = lastNameText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "N/A"
        let age = Int(ageText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "0") ?? 0
        let religion = religionText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "N/A"
        let school = schoolText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "N/A"
        let profession = professionText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "N/A"
        let comment = commentText.text ?? ""
        
        //var pid: String?
        
        var ref: DocumentReference? = nil
        ref = Firestore.firestore().collection("people").addDocument(data: [
            "uid" : PersonManager.shared.uid,
            "vid" : vid ?? "N/A",
            "fid" : fid ?? "N/A",
            "firstName" : firstName,
            "lastName" : lastName,
            "age" : age,
            "religion" : religion,
            "school" : school,
            "education" : education,
            "profession" : profession,
            "comment" : comment,
            "gender" : gender,
            "children" : child,
            "drug" : drug]) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                } else {
                    self.updateFamily(pid: ref!.documentID)
                    print("Document added with ID: \(ref!.documentID)")
                }
        }
        dismiss(animated: true, completion: nil)
    }
    
    func updateFamily(pid: String) {
        var familyRef: DocumentReference? = nil
        familyRef = Firestore.firestore().collection("families").document(fid!)
        familyRef!.updateData([
            "members": FieldValue.arrayUnion([pid])]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Document successfully updated")
                }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "villageSegue" {
            let nav = segue.destination as! UINavigationController
            let controller = nav.topViewController as! VillageSelectViewController
            controller.delegate = self
            controller.previousVillage = villageLabel.text
        } else if segue.identifier == "familySegue" {
            let nav = segue.destination as! UINavigationController
            let controller = nav.topViewController as! FamilySelectViewController
            controller.delegate = self
            controller.previousFamily = familyLabel.text
        } 
    }
}

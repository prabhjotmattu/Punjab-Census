//
//  SettingsViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 6/19/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

class SettingsViewController: UITableViewController {
    
    let loadingAlert = UIAlertController(title: nil, message: "Logging in...", preferredStyle: .alert)
    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    var ref: DocumentReference? = nil
    
    let userDefault = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getUser()
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    func getUser() {
        print("getUser called")
        let user = Auth.auth().currentUser
        if let user = user {
            print("User found")
            let uid = user.uid
            let email = user.email
            
            //nameLabel.text = uid
            emailLabel.text = email
            
            ref = Firestore.firestore().collection("users").document(uid)
            ref?.getDocument { (document, error) in
                if let document = document, document.exists {
                    let name = document.get("name") as? String
                    self.nameLabel.text = name
                } else {
                    print("Document does not exist")
                }
            }
            
        }
        
        
    }

    @IBAction func signOut(_ sender: Any) {
        do {
            try Auth.auth().signOut()
        } catch {
            print("Already logged out")
        }
        self.userDefault.set(false, forKey: "usersignedin")
        self.userDefault.synchronize()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginController = (storyboard.instantiateViewController(identifier: "login")) as! LoginViewController
        UIApplication.shared.windows.first?.rootViewController = loginController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        UIView.transition(with: UIApplication.shared.windows.first!, duration: 0.5, options: [.transitionFlipFromLeft], animations: nil, completion: nil)
    }
    
}

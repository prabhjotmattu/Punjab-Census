//
//  EditVillageViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 5/2/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

class EditVillageViewController: UITableViewController {

    
    var village: Village?

    @IBOutlet weak var villageNameText: UITextField!
    @IBOutlet weak var publicSchoolText: UITextField!
    @IBOutlet weak var privateSchoolText: UITextField!
    @IBOutlet weak var commentText: UITextField!
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var vid: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vid = village?.vid
        villageNameText.text = village?.villageName
        publicSchoolText.text = String(village?.numOfPubSchool ?? 0)
        privateSchoolText.text = String(village?.numOfPrivSchool ?? 0)
        commentText.text = village?.comment
    }
    
    // MARK: - Input Functions
    
    @IBAction func textChanged(_ textField: UITextField) {
        if(textField.tag == 1) {
            if let text = textField.text, let isNum = textField.text?.isNumeric(), !isNum {
                textField.text = String(text.dropLast())
            }
        }
        checkRequired()
    }
    
    func checkRequired() {
        if(villageNameText.hasText && publicSchoolText.hasText && privateSchoolText.hasText) {
            doneButton.isEnabled = true
        } else {
            doneButton.isEnabled = false
        }
    }
    
    //MARK: - Navigation Bar Button Functions
    
    @IBAction func cancel(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func done(_ sender: Any) {
        let villageName = villageNameText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "N/A"
        let publicSchool = Int(publicSchoolText.text ?? "0") ?? 0
        let privateSchool = Int(privateSchoolText.text ?? "0") ?? 0
        let comment = commentText.text ?? ""
        
        var ref: DocumentReference? = nil
        ref = Firestore.firestore().collection("villages").document(vid!)
        ref!.updateData([
            "villageName": villageName,
            "numOfPubSchool": publicSchool,
            "numOfPrivSchool": privateSchool,
            "comment": comment]) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                } else {
                    print("Document added with ID: \(ref!.documentID)")
                }
        }
        navigationController?.popViewController(animated: true)
    }
}

//
//  NewVillageViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 4/4/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

class NewVillageViewController: UITableViewController {
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    @IBOutlet weak var villageNameText: UITextField!
    @IBOutlet weak var publicText: UITextField!
    @IBOutlet weak var privateText: UITextField!
    @IBOutlet weak var commentText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    // MARK: - Input Functions
    
    @IBAction func textChanged(_ textField: UITextField) {
        if(textField.tag == 1) {
            if let text = textField.text, let isNum = textField.text?.isNumeric(), !isNum {
                textField.text = String(text.dropLast())
            }
        }
        checkRequired()
    }
    
    func checkRequired() {
        if(villageNameText.hasText && publicText.hasText && privateText.hasText) {
            doneButton.isEnabled = true
        } else {
            doneButton.isEnabled = false
        }
    }
    
    // MARK: - Navigation Bar Button Functions
    
    @IBAction func cancel() {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func done() {
        let villageName = villageNameText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "N/A"
        let numResidents = 0
        let numPublic = Int(publicText.text ?? "0") ?? 0
        let numPrivate = Int(privateText.text ?? "0") ?? 0
        let comment = commentText.text ?? ""
        
        var ref: DocumentReference? = nil
        ref = Firestore.firestore().collection("villages").addDocument(data: [
            "uid" : VillageManager.shared.uid,
            "villageName" : villageName,
            "numOfResident" : numResidents,
            "numOfPubSchool" : numPublic,
            "numOfPrivSchool" : numPrivate,
            "comment" : comment]) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                } else {
                    print("Document added with ID: \(ref!.documentID)")
                }
        }
        dismiss(animated: true, completion: nil)
    }
}

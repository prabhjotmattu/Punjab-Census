//
//  VillagesMasterViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 3/22/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

protocol VillageSelectionDelegate: class {
    func villageSelected(_ newVillage: Village)
}

class VillagesMasterViewController: UITableViewController {
    
    weak var delegate: VillageSelectionDelegate?
    let searchController = UISearchController(searchResultsController: nil)
    
    var isSearchBarEmpty: Bool { return searchController.searchBar.text?.isEmpty ?? true}
    var isFiltering: Bool { return searchController.isActive && !isSearchBarEmpty}
    
    var lastSelectedRow: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refresh), name: NSNotification.Name(rawValue: "reloadVillageData"), object: nil)
        
        tableView.remembersLastFocusedIndexPath = true
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Villages"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        searchController.view.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        VillageManager.shared.getTitles()
        tableView.reloadData()
        searchController.view.layoutIfNeeded()
        if(lastSelectedRow != nil) {
            let selectedCell = tableView.cellForRow(at: lastSelectedRow!)
            selectedCell?.isHighlighted = true
        }
        print("All villages: \(VillageManager.shared.allVillages)")
    }
    
    @objc func refresh() {
        self.tableView.reloadData()
        setVillageDetailAfterEdit()
    }
    
    func setVillageDetailAfterEdit() {
        if lastSelectedRow != nil {
            if isFiltering {
                if !VillageManager.shared.filteredVillages.isEmpty {
                    let selectedVillage = VillageManager.shared.filteredVillages[lastSelectedRow!.row]
                    delegate?.villageSelected(selectedVillage)
                    //print("Selected Village: \(selectedVillage)")
                }
            } else {
                var sectionVillage: [Village] = []
                for village in VillageManager.shared.allVillages {
                    if VillageManager.shared.titles[lastSelectedRow!.row] == String(village.villageName.first?.uppercased() ?? "") {
                        sectionVillage.append(village)
                    }
                }
                if !sectionVillage.isEmpty {
                    let selectedVillage = sectionVillage[lastSelectedRow!.row]
                    //print("Selected Village: \(selectedVillage)")
                    delegate?.villageSelected(selectedVillage)
                }
            }
            tableView.selectRow(at: lastSelectedRow, animated: true, scrollPosition: .none)
        }

    }
    
    func filterContentForSearchText(_ searchText: String) {
        VillageManager.shared.filteredVillages = VillageManager.shared.allVillages.filter { (village: Village) -> Bool in
            return village.villageName.lowercased().contains(searchText.lowercased())
        }
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return VillageManager.shared.filteredVillages.count
        } else {
            var rows = 0
            for village in VillageManager.shared.allVillages {
                if VillageManager.shared.titles[section] == String(village.villageName.first?.uppercased() ?? "") {
                    rows += 1
                }
            }
            return rows
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isFiltering {
            return ""
        } else {
            return VillageManager.shared.titles[section]
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath)
        
        let nameLabel = cell.viewWithTag(1) as! UILabel
        
        if isFiltering {
            nameLabel.text = VillageManager.shared.filteredVillages[indexPath.row].villageName
            return cell
        } else {
            var sectionVillage: [Village] = []
            for village in VillageManager.shared.allVillages {
                if VillageManager.shared.titles[indexPath.section] == String(village.villageName.first?.uppercased() ?? "") {
                    sectionVillage.append(village)
                }
            }
            nameLabel.text = sectionVillage[indexPath.row].villageName
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(lastSelectedRow != nil) {
            let selectedCell = tableView.cellForRow(at: lastSelectedRow!)
            selectedCell?.isHighlighted = false
        }
        lastSelectedRow = tableView.indexPathForSelectedRow
        
        if isFiltering {
            let selectedVillage = VillageManager.shared.filteredVillages[indexPath.row]
            delegate?.villageSelected(selectedVillage)
            //print("Selected Village: \(selectedVillage)")
        } else {
            var sectionVillage: [Village] = []
            for village in VillageManager.shared.allVillages {
                if VillageManager.shared.titles[indexPath.section] == String(village.villageName.first?.uppercased() ?? "") {
                    sectionVillage.append(village)
                }
            }
            let selectedVillage = sectionVillage[indexPath.row]
            //print("Selected Village: \(selectedVillage)")
            delegate?.villageSelected(selectedVillage)
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if isFiltering {
                let village = VillageManager.shared.filteredVillages[indexPath.row]
                let vid = village.vid
                VillageManager.shared.filteredVillages.remove(at: indexPath.row)
                UIView.transition(with: tableView, duration: 1.0, options: .curveEaseInOut, animations: {self.tableView.reloadData()}, completion: nil)
                let db = Firestore.firestore()
                db.collection("villages").document(vid).delete() { err in
                    if let err = err {
                        print("Error removing document: \(err)")
                    } else {
                        print("Document successfully removed!")
                    }
                }
            } else {
                var sectionVillages: [Village] = []
                for village in VillageManager.shared.allVillages {
                    if VillageManager.shared.titles[indexPath.section] == String(village.villageName.first?.uppercased() ?? "") {
                        sectionVillages.append(village)
                    }
                }
                let vid = sectionVillages[indexPath.row].vid
                let db = Firestore.firestore()
                db.collection("villages").document(vid).delete() { err in
                    if let err = err {
                        print("Error removing document: \(err)")
                    } else {
                        print("Document successfully removed!")
                    }
                }
            }
        }
        tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if isFiltering {
            return 1
        } else {
            return VillageManager.shared.titles.count
        }
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if isFiltering {
            return [""]
        } else {
            return VillageManager.shared.titles
        }
    }
}

extension VillagesMasterViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}

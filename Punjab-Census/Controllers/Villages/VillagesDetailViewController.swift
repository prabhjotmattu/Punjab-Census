//
//  VillagesDetailViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 3/22/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

class VillagesDetailViewController: UITableViewController {
    
    private var populationSnapshotListener: ListenerRegistration?

    
    @IBOutlet weak var residentsLabel: UILabel!
    @IBOutlet weak var publicLabel: UILabel!
    @IBOutlet weak var privateLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var editButton: UIBarButtonItem!
    var initialLoad = false
    
    
    var village: Village? {
        didSet {
            getPopulation()
            refreshUI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editButton.isEnabled = false
    }
    
    private func refreshUI() {
        loadViewIfNeeded()
        
        self.title = village?.villageName
        residentsLabel.text = "\(village?.numOfResident ?? 0)"
        publicLabel.text = "\(village?.numOfPubSchool ?? 0)"
        privateLabel.text = "\(village?.numOfPrivSchool ?? 0)"
        commentLabel.text = village?.comment
    }
    
    // MARK: - Firebase Functions
    
    func getPopulation() {
        loadViewIfNeeded()
        var population = 0
        if let vid = village?.vid {
            populationSnapshotListener?.remove()
            populationSnapshotListener = Firestore.firestore().collection("people").addSnapshotListener {
                (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    do {
                        for person in querySnapshot!.documents {
                            if let personVid = person.get("vid") as? String {
                                if personVid == vid {
                                    population += 1
                                    
                                }
                                
                                self.residentsLabel.text = "\(population)"
                            }
                        }
                        self.updateResidents(vid, population)
                        print("\nPop: \(population)")
                    }
                }
            }
        }
        
    }
    
    func updateResidents(_ vid: String, _ population: Int) {
        let db = Firestore.firestore()
        db.collection("villages").document(vid).updateData([
            "numOfResident": population ]) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                } else {
                    print("Document successully updated")
                }
        }
    }
    
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !initialLoad {
            return 0.0
        } else {
            return 50.0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if !initialLoad {
            return ""
        } else {
            if section == 0 {
                return "Information"
            } else {
                return "Comments"
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editVillageSegue" {
            let controller = segue.destination as! EditVillageViewController
            controller.village = village
        }
    }
    
}

extension VillagesDetailViewController: VillageSelectionDelegate {
    func villageSelected(_ newVillage: Village) {
        //print("Got here \nVillage: \(newVillage)")
        village = newVillage
        if !initialLoad {
            initialLoad = true
            tableView.reloadData()
            editButton.isEnabled = true
        }
    }
}

//
//  FamiliesMasterViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 3/22/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

protocol FamilySelectionDelegate: class {
    func familySelected(_ newFamily: Family)
}

class FamiliesMasterViewController: UITableViewController {
    
    weak var delegate: FamilySelectionDelegate?
    let searchController = UISearchController(searchResultsController: nil)
    
    private var _members: [Person] = []
    var members: [Person] { get {return _members}}
    
    var isSearchBarEmpty: Bool { return searchController.searchBar.text?.isEmpty ?? true}
    var isFiltering: Bool { return searchController.isActive && !isSearchBarEmpty}
    
    var lastSelectedRow: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.refresh), name: NSNotification.Name(rawValue: "reloadFamilyData"), object: nil)
        
        tableView.remembersLastFocusedIndexPath = true
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Families"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        searchController.view.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        FamilyManager.shared.getTitles()
        tableView.reloadData()
        searchController.view.layoutIfNeeded()
        if(lastSelectedRow != nil) {
            let selectedCell = tableView.cellForRow(at: lastSelectedRow!)
            selectedCell?.isHighlighted = true
        }
    }
    
    @objc func refresh() {
        self.tableView.reloadData()
        setFamilyDetailAfterEdit()
    }
    
    func showDeleteError() {
        let alert = UIAlertController(title: "Cannot delete family", message: "Family has members. Remove members before deleting.", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(dismiss)
        self.present(alert, animated: true, completion: nil)
    }
    
    func setFamilyDetailAfterEdit() {
        if lastSelectedRow != nil {
            if isFiltering {
                if !FamilyManager.shared.filteredFamilies.isEmpty {
                    let selectedFamily = FamilyManager.shared.filteredFamilies[lastSelectedRow!.row]
                    delegate?.familySelected(selectedFamily)
                }
                
            } else {
                var sectionFamily: [Family] = []
                for family in FamilyManager.shared.allFamilies {
                    if FamilyManager.shared.titles.indices.contains(lastSelectedRow!.section) {
                        if FamilyManager.shared.titles[lastSelectedRow!.section] == String(family.familyName.first?.uppercased() ?? "") {
                            sectionFamily.append(family)
                        }
                    }
                }
                if !sectionFamily.isEmpty {
                    let selectedFamily = sectionFamily[lastSelectedRow!.row]
                    delegate?.familySelected(selectedFamily)
                }
            }
            tableView.selectRow(at: lastSelectedRow, animated: true, scrollPosition: .none)
        }
    }
    
    func filterContentForSearchText(_ searchText: String) {
        FamilyManager.shared.filteredFamilies = FamilyManager.shared.allFamilies.filter { (family: Family) -> Bool in
            return family.familyName.lowercased().contains(searchText.lowercased())
        }
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return FamilyManager.shared.filteredFamilies.count
        } else {
            var rows = 0
            for family in FamilyManager.shared.allFamilies {
                if FamilyManager.shared.titles[section] == String(family.familyName.first?.uppercased() ?? "") {
                    rows += 1
                }
            }
            return rows
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isFiltering {
            return ""
        } else {
            return FamilyManager.shared.titles[section]
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath)
        
        let nameLabel = cell.viewWithTag(1) as! UILabel
        
        if isFiltering {
            nameLabel.text = FamilyManager.shared.filteredFamilies[indexPath.row].familyName
            return cell
        } else {
            var sectionFamily: [Family] = []
            for family in FamilyManager.shared.allFamilies {
                if FamilyManager.shared.titles[indexPath.section] == String(family.familyName.first?.uppercased() ?? "") {
                    sectionFamily.append(family)
                }
            }
            nameLabel.text = sectionFamily[indexPath.row].familyName
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(lastSelectedRow != nil) {
            let selectedCell = tableView.cellForRow(at: lastSelectedRow!)
            selectedCell?.isHighlighted = false
        }
        lastSelectedRow = tableView.indexPathForSelectedRow
        
        if isFiltering {
            let selectedFamily = FamilyManager.shared.filteredFamilies[indexPath.row]
            delegate?.familySelected(selectedFamily)
        } else {
            var sectionFamily: [Family] = []
            for family in FamilyManager.shared.allFamilies {
                if FamilyManager.shared.titles[indexPath.section] == String(family.familyName.first?.uppercased() ?? "") {
                    sectionFamily.append(family)
                }
            }
            let selectedFamily = sectionFamily[indexPath.row]
            delegate?.familySelected(selectedFamily)
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        
        if editingStyle == .delete {
            if isFiltering {
                let family = FamilyManager.shared.filteredFamilies[indexPath.row]
                if !family.members.isEmpty {
                    showDeleteError()
                } else {
                    let fid = family.fid
                    FamilyManager.shared.filteredFamilies.remove(at: indexPath.row)
                    UIView.transition(with: tableView, duration: 1.0, options: .curveEaseInOut, animations: {self.tableView.reloadData()}, completion: nil)
                    let db = Firestore.firestore()
                    db.collection("families").document(fid).delete() { err in
                        if let err = err {
                            print("Error removing document: \(err)")
                        } else {
                            print("Document successfully removed!")
                        }
                    }
                }
            } else {
                var sectionFamilies: [Family] = []
                for family in FamilyManager.shared.allFamilies {
                    if FamilyManager.shared.titles[indexPath.section] == String(family.familyName.first?.uppercased() ?? "") {
                        sectionFamilies.append(family)
                    }
                }
                let family = sectionFamilies[indexPath.row]
                if !family.members.isEmpty {
                    showDeleteError()
                } else {
                    let fid = sectionFamilies[indexPath.row].fid
                    let db = Firestore.firestore()
                    db.collection("families").document(fid).delete() { err in
                        if let err = err {
                            print("Error removing document: \(err)")
                        } else {
                            print("Document successfully removed!")
                        }
                    }
                }
            }
        }
        tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if isFiltering {
            return 1
        } else {
            return FamilyManager.shared.titles.count
        }
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if isFiltering {
            return [""]
        } else {
            return FamilyManager.shared.titles
        }
    }
}

extension FamiliesMasterViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}

//
//  EditFamilyViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 4/29/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

class EditFamilyViewController: UITableViewController {
    
    var family: Family?
    
    @IBOutlet weak var familyNameText: UITextField!
    @IBOutlet weak var incomeText: UITextField!
    @IBOutlet weak var commentText: UITextField!
    
    @IBOutlet weak var transportationControl: UISegmentedControl!
    @IBOutlet weak var tractorControl: UISegmentedControl!
    @IBOutlet weak var familyControl: UISegmentedControl!
    @IBOutlet weak var policeControl: UISegmentedControl!
    @IBOutlet weak var internetControl: UISegmentedControl!
    @IBOutlet weak var electricityControl: UISegmentedControl!
    @IBOutlet weak var healthcareControl: UISegmentedControl!
    @IBOutlet weak var waterControl: UISegmentedControl!
    @IBOutlet weak var foodControl: UISegmentedControl!
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    var income = 0
    var isClear = true
    
    var transportation = "N/A"
    var tractor = false
    var familyOverseas = false
    var police = false
    var internet = false
    var electricity = false
    var healthcare = false
    var water = false
    var food = false
    var fid: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fid = family?.fid
        familyNameText.text = family?.familyName
        income = Int(family?.income ?? 0)
        incomeText.text = "₹\(income)"
        commentText.text = family?.comment
        
        switch family!.transportation {
        case "Walk":
            transportationControl.selectedSegmentIndex = 0
        case "Bus":
            transportationControl.selectedSegmentIndex = 1
        case "Car":
            transportationControl.selectedSegmentIndex = 2
        default: break
        }
        
        switch family!.tractor {
        case true:
            tractorControl.selectedSegmentIndex = 0
        case false:
            tractorControl.selectedSegmentIndex = 1
        }
        
        switch family!.overseas {
        case true:
            familyControl.selectedSegmentIndex = 0
        case false:
            familyControl.selectedSegmentIndex = 1
        }
        
        switch family!.police {
        case true:
            policeControl.selectedSegmentIndex = 0
        case false:
            policeControl.selectedSegmentIndex = 1
        }
        
        switch family!.internet {
        case true:
            internetControl.selectedSegmentIndex = 0
        case false:
            internetControl.selectedSegmentIndex = 1
        }
        
        switch family!.electricity {
        case true:
            electricityControl.selectedSegmentIndex = 0
        case false:
            electricityControl.selectedSegmentIndex = 1
        }
        
        switch family!.healthcare {
        case true:
            healthcareControl.selectedSegmentIndex = 0
        case false:
            healthcareControl.selectedSegmentIndex = 1
        }
        
        switch family!.water {
        case true:
            waterControl.selectedSegmentIndex = 0
        case false:
            waterControl.selectedSegmentIndex = 1
        }
        
        switch family!.food {
        case true:
            foodControl.selectedSegmentIndex = 0
        case false:
            foodControl.selectedSegmentIndex = 1
        }
        
    }
    
    // MARK: - Segmented Control Functions
    
    @IBAction func switchIndexChanged(_ control: UISegmentedControl) {
        switch control.tag {
        case 1:
            switch control.selectedSegmentIndex {
            case 0:
                transportation = "Walk"
            case 1:
                transportation = "Bus"
            case 2:
                transportation = "Car"
            default:
                transportation = "N/A"
            }
        case 2:
            switch control.selectedSegmentIndex {
            case 0:
                tractor = true
            case 1:
                tractor = false
            default:
                tractor = false
            }
        case 3:
            switch control.selectedSegmentIndex {
            case 0:
                familyOverseas = true
            case 1:
                familyOverseas = false
            default:
                familyOverseas = false
            }
        case 4:
            switch control.selectedSegmentIndex {
            case 0:
                police = true
            case 1:
                police = false
            default:
                police = false
            }
        case 5:
            switch control.selectedSegmentIndex {
            case 0:
                internet = true
            case 1:
                internet = false
            default:
                internet = false
            }
        case 6:
            switch control.selectedSegmentIndex {
            case 0:
                electricity = true
            case 1:
                electricity = false
            default:
                electricity = false
            }
        case 7:
            switch control.selectedSegmentIndex {
            case 0:
                healthcare = true
            case 1:
                healthcare = false
            default:
                healthcare = false
            }
        case 8:
            switch control.selectedSegmentIndex {
            case 0:
                water = true
            case 1:
                water = false
            default:
                water = false
            }
        case 9:
            switch control.selectedSegmentIndex {
            case 0:
                food = true
            case 1:
                food = false
            default:
                food = false
            }
        default:
            break
        }
        checkRequired()
    }
    
    
    // MARK: - Text Functions
    
    @IBAction func textChanged(_ textField: UITextField) {
        if textField.tag == 1 {
            if var text = textField.text {
                if (textField.text?.first == "₹") {
                    text = String(text.dropFirst())
                    if !text.isNumeric() {
                        text = String(text.dropLast())
                    }
                }
                if text.isNumeric() {
                    income = Int(text) ?? 0
                    textField.text = "₹\(text)"
                } else {
                    textField.text = String(text.dropLast())
                }
            }
        }
        checkRequired()
    }
    
    func checkRequired() {
        if(familyNameText.hasText && incomeText.hasText && transportationControl.selectedSegmentIndex != -1 && tractorControl.selectedSegmentIndex != -1 && familyControl.selectedSegmentIndex != -1 && policeControl.selectedSegmentIndex != -1 && internetControl.selectedSegmentIndex != -1 && electricityControl.selectedSegmentIndex != -1 && healthcareControl.selectedSegmentIndex != -1 && waterControl.selectedSegmentIndex != -1 && foodControl.selectedSegmentIndex != -1) {
            doneButton.isEnabled = true
        } else {
            doneButton.isEnabled = false
        }
    }
    
    // MARK: - Navigation Bar Button Functions
    
    @IBAction func cancel(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func done(_ sender: Any) {
        let familyName = familyNameText.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "N/A"
        let familyComment = commentText.text ?? ""
        
        var ref: DocumentReference? = nil
        ref = Firestore.firestore().collection("families").document(fid!)
        ref!.updateData([
            "familyName" : familyName,
            "income" : income,
            "transportation" : transportation,
            "tractor" : tractor,
            "overseas" : familyOverseas,
            "police" : police,
            "internet" : internet,
            "electricity" : electricity,
            "healthcare" : healthcare,
            "water" : water,
            "food" : food,
            "comment" : familyComment]) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                } else {
                    print("Document added with ID: \(ref!.documentID)")
                }
        }
        navigationController?.popViewController(animated: true)
    }
}

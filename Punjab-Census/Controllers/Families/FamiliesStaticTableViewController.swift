//
//  FamiliesStaticTableViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 3/23/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit

class FamiliesStaticTableViewController: UITableViewController {
    
    @IBOutlet weak var incomeLabel: UILabel!
    @IBOutlet weak var transportationLabel: UILabel!
    @IBOutlet weak var tractorLabel: UILabel!
    @IBOutlet weak var familyLabel: UILabel!
    @IBOutlet weak var policeLabel: UILabel!
    @IBOutlet weak var internetLabel: UILabel!
    @IBOutlet weak var electricityLabel: UILabel!
    @IBOutlet weak var healthcareLabel: UILabel!
    @IBOutlet weak var waterLabel: UILabel!
    @IBOutlet weak var foodLabel: UILabel!
    
    @IBOutlet weak var commentLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let parentController = self.parent as! FamiliesDetailViewController
        parentController.saveContainerViewReference(vc: self)
    }

    // MARK: - Table view data source

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

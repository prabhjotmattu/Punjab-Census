//
//  FamiliesDetailViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 3/22/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

class FamiliesDetailViewController: UITableViewController {
    private var membersSnapshotListener: ListenerRegistration?
    var memberIDs: [String] = []
    var members: [Person] = []
    var gottenMembers = false
    var familiesStaticReferenceVC: FamiliesStaticTableViewController?
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var editButton: UIBarButtonItem!
    var initialLoad = false
    
    func saveContainerViewReference(vc:FamiliesStaticTableViewController) {
        self.familiesStaticReferenceVC = vc
    }
    
    var family: Family? {
        didSet {
            if !gottenMembers {
                getMembers()
            } else {
                refreshUI()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editButton.isEnabled = false
        containerView.isHidden = true
        familiesStaticReferenceVC?.view.isHidden = true
        
    }
    
    private func getMembers() {
        loadViewIfNeeded()
        
        memberIDs.removeAll()
        members.removeAll()
        
        if let fam = family {
            memberIDs = fam.members
        }
        
        membersSnapshotListener?.remove()
        membersSnapshotListener = Firestore.firestore().collection("people").addSnapshotListener {
            (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                do {
                    for person in querySnapshot!.documents {
                        if self.memberIDs.contains(person.documentID) {
                            
                            let pid = person.documentID
                            let uid = person.get("uid") as? String ?? "not found"
                            let vid = person.get("vid") as? String ?? "not found"
                            let fid = person.get("fid") as? String ?? "not found"
                            let firstName = person.get("firstName") as? String ?? "not found"
                            let lastName = person.get("lastName") as? String ?? "not found"
                            let age = person.get("age") as? Int ?? 0
                            let gender = person.get("gender") as? String ?? "not found"
                            let religion = person.get("religion") as? String ?? "not found"
                            let children = person.get("children") as? Bool ?? false
                            let drug = person.get("drug") as? Bool ?? false
                            let school = person.get("education") as? String ?? "not found"
                            let education = person.get("education") as? String ?? "not found"
                            let profession = person.get("profession") as? String ?? "not found"
                            let comment = person.get("comment") as? String ?? "not found"
                            let newPerson = Person(uid: uid, pid: pid, vid: vid, fid: fid, firstName: firstName, lastName: lastName, age: age, gender: gender, religion: religion, children: children, drug: drug, school: school, education: education, profession: profession, comment: comment)
                            self.members.append(newPerson)
                            
                        }
                    }
                    //print("Members: \(self.members)")
                }
            }
            self.family?.members = self.memberIDs
            self.tableView.reloadData()
        }
        gottenMembers = true
        
    }
    
    private func refreshUI() {
        loadViewIfNeeded()
        self.title = family?.familyName
        self.familiesStaticReferenceVC?.incomeLabel.text = "₹\(family?.income ?? 0)"
        self.familiesStaticReferenceVC?.transportationLabel.text = family?.transportation
        self.familiesStaticReferenceVC?.commentLabel.text = family?.comment
        
        switch family?.tractor {
        case true:
            self.familiesStaticReferenceVC?.tractorLabel.text = "Yes"
        case false:
            self.familiesStaticReferenceVC?.tractorLabel.text = "No"
        default:
            self.familiesStaticReferenceVC?.tractorLabel.text = "Not Found"
        }
        
        switch family?.overseas {
        case true:
            self.familiesStaticReferenceVC?.familyLabel.text = "Yes"
        case false:
            self.familiesStaticReferenceVC?.familyLabel.text = "No"
        default:
            self.familiesStaticReferenceVC?.familyLabel.text = "Not Found"
        }
        
        switch family?.police {
        case true:
            self.familiesStaticReferenceVC?.policeLabel.text = "Yes"
        case false:
            self.familiesStaticReferenceVC?.policeLabel.text = "No"
        default:
            self.familiesStaticReferenceVC?.policeLabel.text = "Not Found"
        }
        
        switch family?.internet {
        case true:
            self.familiesStaticReferenceVC?.internetLabel.text = "Yes"
        case false:
            self.familiesStaticReferenceVC?.internetLabel.text = "No"
        default:
            self.familiesStaticReferenceVC?.internetLabel.text = "Not Found"
        }
        
        switch family?.electricity {
        case true:
            self.familiesStaticReferenceVC?.electricityLabel.text = "Yes"
        case false:
            self.familiesStaticReferenceVC?.electricityLabel.text = "No"
        default:
            self.familiesStaticReferenceVC?.electricityLabel.text = "Not Found"
        }
        
        switch family?.healthcare {
        case true:
            self.familiesStaticReferenceVC?.healthcareLabel.text = "Yes"
        case false:
            self.familiesStaticReferenceVC?.healthcareLabel.text = "No"
        default:
            self.familiesStaticReferenceVC?.healthcareLabel.text = "Not Found"
        }
        
        switch family?.water {
        case true:
            self.familiesStaticReferenceVC?.waterLabel.text = "Yes"
        case false:
            self.familiesStaticReferenceVC?.waterLabel.text = "No"
        default:
            self.familiesStaticReferenceVC?.waterLabel.text = "Not Found"
        }
        
        switch family?.food {
        case true:
            self.familiesStaticReferenceVC?.foodLabel.text = "Yes"
        case false:
            self.familiesStaticReferenceVC?.foodLabel.text = "No"
        default:
            self.familiesStaticReferenceVC?.foodLabel.text = "Not Found"
        }
        gottenMembers = false
    }
    


    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return memberIDs.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "memberCell", for: indexPath)
        let firstNameLabel = cell.viewWithTag(1) as! UILabel
        let lastNameLabel = cell.viewWithTag(2) as! UILabel
        if !members.isEmpty && members.count == memberIDs.count {
            firstNameLabel.text = members[indexPath.row].firstName
            lastNameLabel.text = members[indexPath.row].lastName
        } else {
            firstNameLabel.text = ""
            lastNameLabel.text = ""
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if !initialLoad {
            return ""
        } else {
            return "Members"
        }
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editFamilySegue" {
            let controller = segue.destination as! EditFamilyViewController
            controller.family = family
        }
    }
    
}

extension FamiliesDetailViewController: FamilySelectionDelegate {
    func familySelected(_ newFamily: Family) {
        family = newFamily
        if !initialLoad {
            initialLoad = true
            containerView.isHidden = false
            tableView.reloadData()
            editButton.isEnabled = true
        }
    }
}

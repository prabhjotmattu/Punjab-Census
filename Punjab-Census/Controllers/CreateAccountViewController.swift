//
//  CreateAccountViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 6/17/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

class CreateAccountViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var codeText: UITextField!
    @IBOutlet weak var eyeButton: UIButton!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var questionButton: UIButton!
    var passwordSecure = true
    
    let loadingAlert = UIAlertController(title: nil, message: "Creating account...", preferredStyle: .alert)
    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        passwordSecure = passwordText.isSecureTextEntry
        createButton.isEnabled = false
        
        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        nameText.setBottomBorder()
        emailText.setBottomBorder()
        passwordText.setBottomBorder()
        codeText.setBottomBorder()
        
        createButton.layer.cornerRadius = 25
        createButton.layer.borderWidth = 1
        createButton.layer.borderColor = UIColor.clear.cgColor
        createButton.contentEdgeInsets = UIEdgeInsets(top: createButton.contentEdgeInsets.top + 15, left: createButton.contentEdgeInsets.left + 15, bottom: createButton.contentEdgeInsets.bottom + 15, right: createButton.contentEdgeInsets.right + 15)
    }
    
    @IBAction func togglePassword(_ sender: UIButton) {
        passwordSecure = !passwordSecure
        passwordText.isSecureTextEntry = passwordSecure
        
        if passwordSecure {
            eyeButton.setImage(UIImage(systemName: "eye.fill"), for: .normal)
        } else {
            eyeButton.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
        }
    }
    
    @IBAction func questionTapped(_ sender: UIButton) {
        let questionAlert = UIAlertController(title: "Access Code", message: "To get an access code contact Jasvir Singh at gomattu@gmail.com", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "OK", style: .default, handler: nil)
        questionAlert.addAction(dismiss)
        present(questionAlert, animated: true, completion: nil)
    }
    
    @IBAction func textChanged(_ textField: UITextField) {
        if nameText.hasText && emailText.hasText && passwordText.hasText && codeText.hasText {
            createButton.isEnabled = true
        }
    }
    
    @IBAction func signUp(_ sender: UIButton) {
        let code = codeText.text
        let docRef = Firestore.firestore().collection("codes").document(code!)
        docRef.getDocument { (document, err) in
            if document!.exists {
                print("Document found")
                self.createUser()
            } else {
                print("Document not found")
                self.showCodeError()
            }
        }
    }
    
    func createUser() {
        Auth.auth().createUser(withEmail: self.emailText.text ?? "", password: self.passwordText.text ?? "") { (result, err) in
            if let err = err {
                print("Error creating user: \(err)")
                self.showAuthError()
            } else {
                print("User successfully created: \(result!)")
                self.addUserInformation(result: result, email: self.emailText.text ?? "", name: self.nameText.text ?? "")
            }
        }
    }
    
    func addUserInformation(result: AuthDataResult?, email: String, name: String) {
        let uid = result?.user.uid
        //var ref: DocumentReference? = nil
        Firestore.firestore().collection("users").document(uid!).setData([
            "email" : email,
            "name" : name ])
        showSuccessAlert()
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func cache(status: FirestoreLoadStatus) {
        switch status {
        case .ready:
            print("Account Created")
            dismiss(animated: true, completion: nil)
        case .userNotLoggedIn:
            showAuthError()
        case .asynchRaceCondition:
            showErrorMessage(title: "A fatal error occurred", message: "Too many calls to cacheHelper.")
        case .corruptDataReceived:
            showErrorMessage(title: "A fatal error occurred", message: "Application recieved malformed data from server. Please check Firebase console and restart application.")
        }
    }
    
    
    func showErrorMessage(title: String?, message: String?) {
        loadingIndicator.removeFromSuperview()
        loadingAlert.title = title
        loadingAlert.message = message
    }
    
    func showCodeError() {
        loadingAlert.dismiss(animated: true, completion: {
            let alert = UIAlertController(title: "Access code not found", message: "Please check your access code and try again", preferredStyle: .alert)
            let dismiss = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(dismiss)
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    func showAuthError() {
        loadingAlert.dismiss(animated: true, completion: {
            let alert = UIAlertController(title: "Email already in use", message: "Please check your input and try again", preferredStyle: .alert)
            let dismiss = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(dismiss)
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    func showSuccessAlert() {
        loadingAlert.dismiss(animated: true, completion: {
            let alert = UIAlertController(title: "User successfully created!", message: "", preferredStyle: .alert)
            let dismiss = UIAlertAction(title: "OK", style: .default, handler: { action in
                self.dismiss(animated: true, completion: nil)
            })
            alert.addAction(dismiss)
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

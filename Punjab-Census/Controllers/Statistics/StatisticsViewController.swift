//
//  StatisticsViewController.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 5/4/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import SwiftUI
import Firebase

class StatisticsViewController: UITableViewController, ObservableObject {
    
    
    //@Published var peopleQueriesGotten: Bool = false
    @Published var test: Int = 20
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    
    // MARK: - People Statistics Data
    
    // Age variables
    @Published var avgAge: Int = 0
    @Published var under18: Int = 0
    @Published var between18and39: Int = 0
    @Published var between40and64: Int = 0
    @Published var over65: Int = 0
    
    // Education variables
    @Published var attending: Int = 0
    @Published var highSchool: Int = 0
    @Published var bachelors: Int = 0
    @Published var masters: Int = 0
    @Published var na: Int = 0
    
    // Gender Variables
    @Published var male: Double = 0
    @Published var female: Double = 0
    
    // Religion Variables
    @Published var religionNames: [String] = []
    @Published var religionCounts: [Double] = []
    @Published var religions: [String:Double] = [:]
    
    // Children Variables
    @Published var yesChildren: Double = 0
    @Published var noChildren: Double = 0
    
    // Drug/Alchol Variables
    @Published var yesDrugs: Double = 0
    @Published var noDrugs: Double = 0
    
    
    func getAgeData(query: Query, range: String) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                var count = 0
                var age = 0
                for person in querySnapshot!.documents {
                    count += 1
                    if range == "average" {
                        age += person.get("age") as? Int ?? 0
                    }
                }
                
                switch range {
                case "average":
                    if count != 0 {
                        self.avgAge = age/count
                    } else {
                        self.avgAge = 0
                    }
                    //self.avgAge = age/count
                case "lessThan18":
                    self.under18 = count
                case "between18and39":
                    self.between18and39 = count
                case "between40and64":
                    self.between40and64 = count
                case "over65":
                    self.over65 = count
                default:
                    break
                }
            }
        }
    }
    
    func getEducationData(query: Query, education: String) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                var count = 0
                for _ in querySnapshot!.documents {
                    count += 1
                }
                switch education {
                case "attending":
                    self.attending = count
                case "highSchool":
                    self.highSchool = count
                case "bachelors":
                    self.bachelors = count
                case "masters":
                    self.masters = count
                case "n/a":
                    self.na = count
                    //print("N/A is found")
                default:
                    break
                }
            }
        }
    }
    
    func getGenderData(query: Query, gender: String) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                var count: Double = 0
                for _ in querySnapshot!.documents {
                    count += 1
                }
                switch gender {
                case "Male":
                    self.male = count
                case "Female":
                    self.female = count
                default:
                    break
                }
            }
        }
    }
    
    func getReligionData(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.religions = [:]
                for person in querySnapshot!.documents {
                    if let religion = person.get("religion") as? String {
                        if let count = self.religions[religion] {
                            self.religions[religion] = count + 1
                        } else {
                            self.religions[religion] = 1
                        }
                    }
                }
                self.religionNames = Array<String>(self.religions.keys)
                self.religionCounts = Array<Double>(self.religions.values)
            }
        }
    }
    
    func getChildrenData(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.yesChildren = 0
                self.noChildren = 0
                for person in querySnapshot!.documents {
                    if let hasChildren = person.get("children") as? Bool {
                        if hasChildren {
                            self.yesChildren += 1
                        } else {
                            self.noChildren += 1
                        }
                    }
                }
            }
        }
    }
    
    func getDrugData(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.yesDrugs = 0
                self.noDrugs = 0
                for person in querySnapshot!.documents {
                    if let drugUse = person.get("drug") as? Bool {
                        if drugUse {
                            self.yesDrugs += 1
                        } else {
                            self.noDrugs += 1
                        }
                    }
                }
            }
        }
    }
    
    func getPeopleQueries(vid: String = "All Villages") {
        let ref = Firestore.firestore().collection("people").whereField("uid", isEqualTo: PersonManager.shared.uid)
        
        //print("getPeopleQueries called \nVillage: \(vid)")
        
        if vid != "All Villages" {
            // Age Queries
            getAgeData(query: ref.whereField("vid", isEqualTo: vid), range: "average")
            getAgeData(query: ref.whereField("age", isLessThan: 18).whereField("vid", isEqualTo: vid), range: "lessThan18")
            getAgeData(query: ref.whereField("age", isGreaterThanOrEqualTo: 18)
                .whereField("age", isLessThan: 40).whereField("vid", isEqualTo: vid),
                       range: "between18and39")
            getAgeData(query: ref.whereField("age", isGreaterThanOrEqualTo: 40)
                .whereField("age", isLessThan: 65).whereField("vid", isEqualTo: vid),
                       range: "between40and64")
            getAgeData(query: ref.whereField("age", isGreaterThanOrEqualTo: 65).whereField("vid", isEqualTo: vid), range: "over65")
            
            // Gender Queries
            getGenderData(query: ref.whereField("gender", isEqualTo: "Male").whereField("vid", isEqualTo: vid), gender: "Male")
            getGenderData(query: ref.whereField("gender", isEqualTo: "Female").whereField("vid", isEqualTo: vid), gender: "Female")
            
            // Religion Queries
            getReligionData(query: ref.whereField("vid", isEqualTo: vid))
            
            // Children Queries
            getChildrenData(query: ref.whereField("vid", isEqualTo: vid))
            
            // Drug/Alcohol Queries
            getDrugData(query: ref.whereField("vid", isEqualTo: vid))
            
            // Education Queries
            getEducationData(query: ref.whereField("education", isEqualTo: "Attending School").whereField("vid", isEqualTo: vid), education: "attending")
            getEducationData(query: ref.whereField("education", isEqualTo: "High School").whereField("vid", isEqualTo: vid), education: "highSchool")
            getEducationData(query: ref.whereField("education", isEqualTo: "Bachelors").whereField("vid", isEqualTo: vid), education: "bachelors")
            getEducationData(query: ref.whereField("education", isEqualTo: "Masters").whereField("vid", isEqualTo: vid), education: "masters")
            getEducationData(query: ref.whereField("education", isEqualTo: "N/A").whereField("vid", isEqualTo: vid), education: "n/a")
        } else {
            // Age Queries
            getAgeData(query: ref, range: "average")
            getAgeData(query: ref.whereField("age", isLessThan: 18), range: "lessThan18")
            getAgeData(query: ref.whereField("age", isGreaterThanOrEqualTo: 18)
                .whereField("age", isLessThan: 40),
                       range: "between18and39")
            getAgeData(query: ref.whereField("age", isGreaterThanOrEqualTo: 40)
                .whereField("age", isLessThan: 65),
                       range: "between40and64")
            getAgeData(query: ref.whereField("age", isGreaterThanOrEqualTo: 65), range: "over65")
            
            // Gender Queries
            getGenderData(query: ref.whereField("gender", isEqualTo: "Male"), gender: "Male")
            getGenderData(query: ref.whereField("gender", isEqualTo: "Female"), gender: "Female")
            
            // Religion Queries
            getReligionData(query: ref)
            
            // Children Queries
            getChildrenData(query: ref)
            
            // Drug/Alcohol Queries
            getDrugData(query: ref)
            
            // Education Queries
            getEducationData(query: ref.whereField("education", isEqualTo: "Attending School"), education: "attending")
            getEducationData(query: ref.whereField("education", isEqualTo: "High School"), education: "highSchool")
            getEducationData(query: ref.whereField("education", isEqualTo: "Bachelors"), education: "bachelors")
            getEducationData(query: ref.whereField("education", isEqualTo: "Masters"), education: "masters")
            getEducationData(query: ref.whereField("education", isEqualTo: "N/A"), education: "n/a")
        }
        
        
        getVillages()
        /*
         getData(query: ref, value: "gender")
         getData(query: ref, value: "education")
         getData(query: ref, value: "religion")
         */
    }
    
    @Published var villages: [String:String] = [:]
    //@Published var villageNames: [String] = []
    @Published var villageKeys: [String] = []
    
    func getVillages() {
        let ref = Firestore.firestore().collection("villages").whereField("uid", isEqualTo: VillageManager.shared.uid)
        
        ref.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting doucments: \(err)")
            } else {
                self.villages = [:]
                self.villageKeys = []
                
                self.villages["All Villages"] = "All Villages"
                self.villageKeys.append("All Villages")
                
                for village in querySnapshot!.documents {
                    //print("getVillages called")
                    if let villageName = village.get("villageName") as? String {
                        //print(villageName)
                        let vid = village.documentID
                        self.villages[vid] = villageName
                        //self.villageNames.append(villageName)
                        self.villageKeys.append(vid)
                    }
                }
                //print(self.villages)
            }
        }
    }
    
    // MARK: - Family Statistics Data
    
    @Published var familyNames: [String] = []
    
    // Income Variables
    @Published var incomesPie: [String:Double] = [:]
    @Published var incomesPieData: [Double] = []
    @Published var incomes: [String:Double] = [:]
    @Published var avgIncome: Double = 0
    
    // Family Size Variables
    @Published var familySizes: [String:Double] = [:]
    @Published var familySizePie: [String:Double] = [:]
    @Published var familySizePieData: [Double] = []
    @Published var avgFamilySize: Double = 0
    
    // Family Transportation Variables
    @Published var transportationCounts: [String:Double] = [:]
    @Published var transportationWalkFamilies: [String] = []
    @Published var transportationBusFamilies: [String] = []
    @Published var transportationCarFamilies: [String] = []
    @Published var transportationPieData: [Double] = []
    
    // Family Tractor Variables
    @Published var yesTractor: Double = 0
    @Published var noTractor: Double = 0
    @Published var yesTractorFamilies: [String] = []
    @Published var noTractorFamilies: [String] = []
    
    // Family Overseas Variables
    @Published var yesOverseas: Double = 0
    @Published var noOverseas: Double = 0
    @Published var yesOverseasFamilies: [String] = []
    @Published var noOverseasFamilies: [String] = []
    
    // Family Police Variables
    @Published var yesPolice: Double = 0
    @Published var noPolice: Double = 0
    @Published var yesPoliceFamilies: [String] = []
    @Published var noPoliceFamilies: [String] = []
    
    // Family Internet Variables
    @Published var yesInternet: Double = 0
    @Published var noInternet: Double = 0
    @Published var yesInternetFamilies: [String] = []
    @Published var noInternetFamilies: [String] = []
    
    // Family Electricity Variables
    @Published var yesElectricity: Double = 0
    @Published var noElectricity: Double = 0
    @Published var yesElectricityFamilies: [String] = []
    @Published var noElectricityFamilies: [String] = []
    
    // Family Healthcare Variables
    @Published var yesHealthcare: Double = 0
    @Published var noHealthcare: Double = 0
    @Published var yesHealthcareFamilies: [String] = []
    @Published var noHealthcareFamilies: [String] = []
    
    // Family Water Variables
    @Published var yesWater: Double = 0
    @Published var noWater: Double = 0
    @Published var yesWaterFamilies: [String] = []
    @Published var noWaterFamilies: [String] = []
    
    // Family Food Variables
    @Published var yesFood: Double = 0
    @Published var noFood: Double = 0
    @Published var yesFoodFamilies: [String] = []
    @Published var noFoodFamilies: [String] = []
    
    func getIncomeData(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                var count = 0.0
                var totalIncome = 0.0
                self.incomesPie = ["under10":0 ,"between10and40":0 ,"over40":0]
                
                for family in querySnapshot!.documents {
                    if let familyName = family.get("familyName") as? String, let income = family.get("income") as? Double {
                        //Bar Graph Data
                        count += 1
                        totalIncome += income
                        self.incomes[familyName] = income
                        
                        // Pie Chart Data
                        if income <= 10000 {
                            if let count = self.incomesPie["under10"] {
                                self.incomesPie["under10"] = count + 1
                            } else {
                                self.incomesPie["under10"] = 1
                            }
                        } else if income > 10000 && income <= 40000 {
                            if let count = self.incomesPie["between10and40"] {
                                self.incomesPie["between10and40"] = count + 1
                            } else {
                                self.incomesPie["between10and40"] = 1
                            }
                        } else if income >= 40000 {
                            if let count = self.incomesPie["over40"] {
                                self.incomesPie["over40"] = count + 1
                            } else {
                                self.incomesPie["over40"] = 1
                            }
                        }
                    }
                }
                self.familyNames = Array(self.incomes.keys)
                self.avgIncome = totalIncome / count
                self.incomesPieData = [self.incomesPie["under10"] ?? 0, self.incomesPie["between10and40"] ?? 0, self.incomesPie["over40"] ?? 0]
                //print("Incomes: \(self.incomesPieData)")
                //print("Family Names: \(self.familyNames)")
            }
        }
    }
    
    func getFamilySize(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                var count = 0.0
                var totalMembers = 0.0
                for family in querySnapshot!.documents {
                    if let familyName = family.get("familyName") as? String, let familyMembers = family.get("members") as? [String] {
                        
                        //Bar Graph Data
                        count += 1
                        let familySize = familyMembers.count
                        totalMembers += Double(familySize)
                        self.familySizes[familyName] = Double(familySize)
                        self.familySizePie = ["under3":0 ,"between3and6":0 ,"over6":0]
                        //self.familySizePieData = []
                        
                        // Pie Chart Data
                        if familySize <= 2 {
                            if let count = self.familySizePie["under3"] {
                                self.familySizePie["under3"] = count + 1
                            } else {
                                self.familySizePie["under3"] = 1
                            }
                        } else if familySize >= 3 && familySize <= 6 {
                            if let count = self.familySizePie["between3and6"] {
                                self.familySizePie["between3and6"] = count + 1
                            } else {
                                self.familySizePie["between3and6"] = 1
                            }
                        } else if familySize >= 7 {
                            if let count = self.familySizePie["over6"] {
                                self.familySizePie["over6"] = count + 1
                            } else {
                                self.familySizePie["over6"] = 1
                            }
                        }
                    }
                }
                self.familyNames = Array(self.familySizes.keys)
                self.avgFamilySize = totalMembers / count
                self.familySizePieData = [self.familySizePie["under3"] ?? 0, self.familySizePie["between3and6"] ?? 0, self.familySizePie["over6"] ?? 0]
                //print("Size: \(self.familySizePieData)")
                //print("Sizes: \(self.familySizes)")
                //print("Family Names: \(self.familyNames)")
            }
        }
    }
    
    func getFamilyTransportation(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.transportationWalkFamilies = []
                self.transportationBusFamilies = []
                self.transportationCarFamilies = []
                self.transportationCounts = ["Walk":0, "Bus":0, "Car":0]
                for family in querySnapshot!.documents {
                    if let familyName = family.get("familyName") as? String, let familyTransportation = family.get("transportation") as? String {
                        
                        if familyTransportation == "Walk" {
                            self.transportationWalkFamilies.append(familyName)
                            if let count = self.transportationCounts["Walk"] {
                                self.transportationCounts["Walk"] = count + 1
                            } else {
                                self.transportationCounts["Walk"] = 1
                            }
                        } else if familyTransportation == "Bus" {
                            self.transportationBusFamilies.append(familyName)
                            if let count = self.transportationCounts["Bus"] {
                                self.transportationCounts["Bus"] = count + 1
                            } else {
                                self.transportationCounts["Bus"] = 1
                            }
                        } else if familyTransportation == "Car" {
                            self.transportationCarFamilies.append(familyName)
                            if let count = self.transportationCounts["Car"] {
                                self.transportationCounts["Car"] = count + 1
                            } else {
                                self.transportationCounts["Car"] = 1
                            }
                        }
                    }
                }
                self.transportationPieData = [self.transportationCounts["Walk"] ?? 0, self.transportationCounts["Bus"] ?? 0, self.transportationCounts["Car"] ?? 0]
            }
        }
    }
    
    func getFamilyTractor(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.yesTractor = 0
                self.noTractor = 0
                self.yesTractorFamilies = []
                self.noTractorFamilies = []
                for family in querySnapshot!.documents {
                    if let familyName = family.get("familyName") as? String, let tractor = family.get("tractor") as? Bool {
                        if tractor {
                            self.yesTractor += 1
                            self.yesTractorFamilies.append(familyName)
                        } else if !tractor {
                            self.noTractor += 1
                            self.noTractorFamilies.append(familyName)
                        }
                    }
                }
            }
        }
    }
    
    func getFamilyOverseas(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.yesOverseas = 0
                self.noOverseas = 0
                self.yesOverseasFamilies = []
                self.noOverseasFamilies = []
                for family in querySnapshot!.documents {
                    if let familyName = family.get("familyName") as? String, let overseas = family.get("overseas") as? Bool {
                        if overseas {
                            self.yesOverseas += 1
                            self.yesOverseasFamilies.append(familyName)
                        } else if !overseas {
                            self.noOverseas += 1
                            self.noOverseasFamilies.append(familyName)
                        }
                    }
                }
            }
        }
    }
    
    func getFamilyPolice(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.yesPolice = 0
                self.noPolice = 0
                self.yesPoliceFamilies = []
                self.noPoliceFamilies = []
                for family in querySnapshot!.documents {
                    if let familyName = family.get("familyName") as? String, let police = family.get("police") as? Bool {
                        if police {
                            self.yesPolice += 1
                            self.yesPoliceFamilies.append(familyName)
                        } else if !police {
                            self.noPolice += 1
                            self.noPoliceFamilies.append(familyName)
                        }
                    }
                }
            }
        }
    }
    
    func getFamilyInternet(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.yesInternet = 0
                self.noInternet = 0
                self.yesInternetFamilies = []
                self.noInternetFamilies = []
                for family in querySnapshot!.documents {
                    if let familyName = family.get("familyName") as? String, let internet = family.get("internet") as? Bool {
                        if internet {
                            self.yesInternet += 1
                            self.yesInternetFamilies.append(familyName)
                        } else if !internet {
                            self.noInternet += 1
                            self.noInternetFamilies.append(familyName)
                        }
                    }
                }
            }
        }
    }
    
    func getFamilyElectricity(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.yesElectricity = 0
                self.noElectricity = 0
                self.yesElectricityFamilies = []
                self.noElectricityFamilies = []
                for family in querySnapshot!.documents {
                    if let familyName = family.get("familyName") as? String, let electricity = family.get("electricity") as? Bool {
                        if electricity {
                            self.yesElectricity += 1
                            self.yesElectricityFamilies.append(familyName)
                        } else if !electricity {
                            self.noElectricity += 1
                            self.noElectricityFamilies.append(familyName)
                        }
                    }
                }
            }
        }
    }
    
    func getFamilyHealthcare(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.yesHealthcare = 0
                self.noHealthcare = 0
                self.yesHealthcareFamilies = []
                self.noHealthcareFamilies = []
                for family in querySnapshot!.documents {
                    if let familyName = family.get("familyName") as? String, let healthcare = family.get("healthcare") as? Bool {
                        if healthcare {
                            self.yesHealthcare += 1
                            self.yesHealthcareFamilies.append(familyName)
                        } else if !healthcare {
                            self.noHealthcare += 1
                            self.noHealthcareFamilies.append(familyName)
                        }
                    }
                }
            }
        }
    }
    
    func getFamilyWater(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.yesWater = 0
                self.noWater = 0
                self.yesWaterFamilies = []
                self.noWaterFamilies = []
                for family in querySnapshot!.documents {
                    if let familyName = family.get("familyName") as? String, let water = family.get("water") as? Bool {
                        if water {
                            self.yesWater += 1
                            self.yesWaterFamilies.append(familyName)
                        } else if !water {
                            self.noWater += 1
                            self.noWaterFamilies.append(familyName)
                        }
                    }
                }
            }
        }
    }
    
    func getFamilyFood(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                self.yesFood = 0
                self.noFood = 0
                self.yesFoodFamilies = []
                self.noFoodFamilies = []
                for family in querySnapshot!.documents {
                    if let familyName = family.get("familyName") as? String, let food = family.get("food") as? Bool {
                        if food {
                            self.yesFood += 1
                            self.yesFoodFamilies.append(familyName)
                        } else if !food {
                            self.noFood += 1
                            self.noFoodFamilies.append(familyName)
                        }
                    }
                }
            }
        }
    }
    
    func getFamilyQueries() {
        let ref = Firestore.firestore().collection("families").whereField("uid", isEqualTo: FamilyManager.shared.uid)
        
        getIncomeData(query: ref)
        //getFamilySize(query: ref)
        getFamilyTransportation(query: ref)
        getFamilyTractor(query: ref)
        getFamilyOverseas(query: ref)
        getFamilyPolice(query: ref)
        getFamilyInternet(query: ref)
        getFamilyElectricity(query: ref)
        getFamilyHealthcare(query: ref)
        getFamilyWater(query: ref)
        getFamilyFood(query: ref)
    }
    
    // MARK: - Village Statistics Data
    
    @Published var villageNames: [String] = []
    
    // Public School Variables
    @Published var publicSchoolPie: [String:Double] = [:]
    @Published var publicSchoolPieData: [Double] = []
    @Published var publicSchools: [String:Double] = [:]
    
    // Public School Variables
    @Published var privateSchoolPie: [String:Double] = [:]
    @Published var privateSchoolPieData: [Double] = []
    @Published var privateSchools: [String:Double] = [:]
    
    // Population Variables
    @Published var populationPie: [String:Double] = [:]
    @Published var populationPieData: [Double] = []
    @Published var populations: [String:Double] = [:]
    
    func getVillageSchools(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                //var count = 0.0
                //var totalSchools = 0.0
                self.publicSchoolPie = ["under3":0 ,"between3and6":0 ,"over6":0]
                self.privateSchoolPie = ["under3":0 ,"between3and6":0 ,"over6":0]
                //self.publicSchoolPieData = []
                
                for village in querySnapshot!.documents {
                    if let villageName = village.get("villageName") as? String, let publicSchoolsCount = village.get("numOfPubSchool") as? Double, let privateSchoolsCount = village.get("numOfPrivSchool") as? Double {
                        
                        //Bar Graph Data
                        //count += 1
                        //let familySize = publicSchoolsCount.count
                        //totalSchools += Double(familySize)
                        self.publicSchools[villageName] = Double(publicSchoolsCount)
                        self.privateSchools[villageName] = Double(privateSchoolsCount)
                        
                        // Public School Pie Chart Data
                        if publicSchoolsCount <= 2 {
                            if let count = self.publicSchoolPie["under3"] {
                                self.publicSchoolPie["under3"] = count + 1
                            } else {
                                self.publicSchoolPie["under3"] = 1
                            }
                        } else if publicSchoolsCount >= 3 && publicSchoolsCount <= 6 {
                            if let count = self.publicSchoolPie["between3and6"] {
                                self.publicSchoolPie["between3and6"] = count + 1
                            } else {
                                self.publicSchoolPie["between3and6"] = 1
                            }
                        } else if publicSchoolsCount >= 7 {
                            if let count = self.publicSchoolPie["over6"] {
                                self.publicSchoolPie["over6"] = count + 1
                            } else {
                                self.publicSchoolPie["over6"] = 1
                            }
                        }
                        
                        // Private School Pie Chart Data
                        if privateSchoolsCount <= 2 {
                            if let count = self.privateSchoolPie["under3"] {
                                self.privateSchoolPie["under3"] = count + 1
                            } else {
                                self.privateSchoolPie["under3"] = 1
                            }
                        } else if privateSchoolsCount >= 3 && privateSchoolsCount <= 6 {
                            if let count = self.privateSchoolPie["between3and6"] {
                                self.privateSchoolPie["between3and6"] = count + 1
                            } else {
                                self.privateSchoolPie["between3and6"] = 1
                            }
                        } else if privateSchoolsCount >= 7 {
                            if let count = self.privateSchoolPie["over6"] {
                                self.privateSchoolPie["over6"] = count + 1
                            } else {
                                self.privateSchoolPie["over6"] = 1
                            }
                        }
                    }
                }
                
                self.villageNames = Array(self.publicSchools.keys)
                
                self.publicSchoolPieData = [self.publicSchoolPie["under3"] ?? 0, self.publicSchoolPie["between3and6"] ?? 0, self.publicSchoolPie["over6"] ?? 0]
                self.privateSchoolPieData = [self.privateSchoolPie["under3"] ?? 0, self.privateSchoolPie["between3and6"] ?? 0, self.privateSchoolPie["over6"] ?? 0]
                //print("Pub Data: \(self.publicSchoolPieData)")
                //print("Family Names: \(self.familyNames)")
            }
        }
    }
    
    func getVillagePopulations(query: Query) {
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                //var count = 0.0
                //var totalSchools = 0.0
                self.populationPie = ["under25":0 ,"between25and50":0 ,"over50":0]
                //self.privateSchoolPie = ["under3":0 ,"between3and6":0 ,"over6":0]
                //self.publicSchoolPieData = []
                
                for village in querySnapshot!.documents {
                    if let villageName = village.get("villageName") as? String, let population = village.get("numOfResident") as? Double {
                        
                        //Bar Graph Data
                        //count += 1
                        //let familySize = publicSchoolsCount.count
                        //totalSchools += Double(familySize)
                        self.populations[villageName] = Double(population)
                        
                        // Public School Pie Chart Data
                        if population <= 25 {
                            if let count = self.populationPie["under25"] {
                                self.populationPie["under25"] = count + 1
                            } else {
                                self.populationPie["under25"] = 1
                            }
                        } else if population >= 25 && population <= 50 {
                            if let count = self.populationPie["between25and50"] {
                                self.populationPie["between25and50"] = count + 1
                            } else {
                                self.populationPie["between25and50"] = 1
                            }
                        } else if population > 50 {
                            if let count = self.populationPie["over50"] {
                                self.populationPie["over50"] = count + 1
                            } else {
                                self.populationPie["over50"] = 1
                            }
                        }
                    }
                }
                
                self.villageNames = Array(self.populations.keys)
                
                self.populationPieData = [self.populationPie["under25"] ?? 0, self.populationPie["between25and50"] ?? 0, self.populationPie["over50"] ?? 0]
                //print("Pub Data: \(self.publicSchoolPieData)")
                //print("Family Names: \(self.familyNames)")
            }
        }
    }
    
    
    @IBSegueAction func addSwiftUIPersonView(_ coder: NSCoder) -> UIViewController? {
        let personHostingController = UIHostingController(coder: coder, rootView: PersonStatisticsUIView())
        personHostingController!.view.backgroundColor = UIColor.clear
        
        return personHostingController
    }
    
    @IBSegueAction func addSwiftUIFamilyView(_ coder: NSCoder) -> UIViewController? {
        let familyHostingController = UIHostingController(coder: coder, rootView: FamilyStatisticsUIView())
        familyHostingController!.view.backgroundColor = UIColor.clear
        
        return familyHostingController
    }
    
    @IBSegueAction func addSwiftUIVillageView(_ coder: NSCoder) -> UIViewController? {
        let villageHostingController = UIHostingController(coder: coder, rootView: VillageStatisticsUIView())
        villageHostingController!.view.backgroundColor = UIColor.clear
        
        return villageHostingController
    }
    
    
}



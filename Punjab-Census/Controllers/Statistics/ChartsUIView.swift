//
//  ChartsUIView.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 5/7/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import SwiftUI

struct BarView: View {
    
    var barName: String
    var value: Int
    
    var body: some View {
        VStack {
            ZStack(alignment: .bottom) {
                RoundedRectangle(cornerRadius: 10, style: .continuous)
                    .frame(width: 30, height: 200)
                    .foregroundColor(Color(#colorLiteral(red: 0.1510860026, green: 0.4282727242, blue: 0.6223948598, alpha: 1)))
                if value > 200 {
                    RoundedRectangle(cornerRadius: 10, style: .continuous)
                    .frame(width: 30, height: CGFloat(value/500))
                    .foregroundColor(Color(#colorLiteral(red: 0.03921568627, green: 0.5176470588, blue: 1, alpha: 1)))
                } else if value < 200 {
                    RoundedRectangle(cornerRadius: 10, style: .continuous)
                    .frame(width: 30, height: CGFloat(value))
                    .foregroundColor(Color(#colorLiteral(red: 0.03921568627, green: 0.5176470588, blue: 1, alpha: 1)))
                }
                Text("\(value)")
                    .fontWeight(.light)
                    .padding(.bottom, 80)
                    .foregroundColor(.white)
            }
            Text(barName).padding(.top, 8)
        }
    }
}

struct HBarView: View {
    var barName: String
    var value: Double
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(barName).padding(.horizontal, 24)
            HStack {
                
                ZStack(alignment: .leading) {
                    RoundedRectangle(cornerRadius: 10, style: .continuous)
                        .frame(width: 500, height: 30)
                        .foregroundColor(Color(#colorLiteral(red: 0.1510860026, green: 0.4282727242, blue: 0.6223948598, alpha: 1)))
                    RoundedRectangle(cornerRadius: 10, style: .continuous)
                        .frame(width: CGFloat(value.squareRoot()), height: 30)
                        .foregroundColor(Color(#colorLiteral(red: 0.03921568627, green: 0.5176470588, blue: 1, alpha: 1)))
                    
                    /*
                    if value > 50000 {
                        RoundedRectangle(cornerRadius: 10, style: .continuous)
                            .frame(width: CGFloat(Int(value.squareRoot())), height: 30)
                        .foregroundColor(Color(#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)))
                    } else if value < 50000 && value > 10000 {
                        RoundedRectangle(cornerRadius: 10, style: .continuous)
                            .frame(width: CGFloat(value.squareRoot()), height: 30)
                        .foregroundColor(Color(#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)))
                    } else {
                        RoundedRectangle(cornerRadius: 10, style: .continuous)
                            .frame(width: CGFloat(value.squareRoot()), height: 30)
                        .foregroundColor(Color(#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)))
                    }
 */
                    
                    
                    Text("\(Int(value))")
                        .fontWeight(.light)
                        .padding(.leading, 80)
                        .foregroundColor(.white)
                }
                
            }
        }.padding(.bottom, 24)
        
    }
}


struct PieViewSegment: View {
    var geometry: GeometryProxy
    var slideData: SlideData
    @State private var show: Bool = false
    var index: Int
    var colors: [Color] = [Color(#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)), Color(#colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)), Color(#colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1))]
    
    var path: Path {
        let chartSize = geometry.size.width
        let radius = chartSize / 2
        let centerX = radius
        let centerY = radius
        
        var path = Path()
        path.move(to: CGPoint(x: centerX, y: centerY))
        path.addArc(center: CGPoint(x: centerX, y: centerY),
                    radius: radius,
                    startAngle: slideData.startAngle,
                    endAngle: slideData.endAngle,
                    clockwise: false)
        return path
    }
    
    public var body: some View {
        
        path.fill(colors[slideData.index])
            .overlay(path.stroke(Color.white, lineWidth: 1))
        .scaleEffect(self.show ? 1 : 0)
        .animation(
                Animation.spring(response: 0.5, dampingFraction: 0.5, blendDuration: 0.3)
                    .delay(Double(self.index) * 0.03)
        ).onAppear() {
                self.show = true
        }
    }
}

struct PieView: View {
    var pieChartData: PieChartData
    var annotations: [String]
    
    var body: some View {
        GeometryReader { geometry in
            self.makePieChart(geometry, pieChartData: self.pieChartData.data)
        }
    }
    
    func makePieChart(_ geometry: GeometryProxy, pieChartData: [SlideData]) -> some View {
        let chartSize = min(geometry.size.width, geometry.size.height)
        let radius = chartSize / 2
        let centerX = geometry.size.width / 2
        let centerY = geometry.size.height / 2
        //var index = 0
        return ZStack {
            ForEach(0..<pieChartData.count, id: \.self) { index in
                PieViewSegment(geometry: geometry, slideData: pieChartData[index], index: index)
            }
            
            ForEach(pieChartData) { slideData in
                if slideData.percentage != "0.0" {
                    Text("\(self.annotations[slideData.index]): \n\(slideData.percentage)%")
                    .foregroundColor(Color.white)
                    .bold()
                    .position(CGPoint(x: centerX + slideData.annotationDeltaX*radius,
                                      y: centerY + slideData.annotationDeltaY*radius))
                }
            }
        }
    }
    
    func doThing(index: Int) -> Int {
        return index + 1
    }
    
}

struct ChartsUIView_Previews: PreviewProvider {
    static var previews: some View {
        PieView(pieChartData: PieChartData(data: [1, 2, 3]), annotations: ["Male", "Female", "N/A"])
        
    }
}

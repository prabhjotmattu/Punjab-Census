//
//  DataItem.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 5/7/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import Foundation
import SwiftUI

class DataItem {
    var name: String! = ""
    var value: Double = 0.0
    var color: Color! = .blue
    var highlighted: Bool = false
    
    init(name: String, value: Double, color: Color? = nil) {
        self.name = name
        self.value = value
        
        if let color = color {
            self.color = color
        } else {
            self.color = .random()
        }
    }
}

class SlideData: Identifiable, ObservableObject {
    let id: UUID = UUID()
    var data: DataItem!
    var percentage: String! = ""
    var startAngle: Angle! = .degrees(0)
    var endAngle: Angle! = .degrees(0)
    
    var index: Int = 0
    var annotation: String! = ""
    var annotationDeltaX: CGFloat! = 0.0
    var annotationDeltaY: CGFloat! = 0.0
    
    var deltaX: CGFloat! = 0.0
    var deltaY: CGFloat! = 0.0
    
    init() { }
    
    init(startAngle: Angle, endAngle: Angle) {
        
        self.data = DataItem(name: "", value: 0)
        self.startAngle = startAngle
        self.endAngle = endAngle
    }
}

//
//  VillageStatisticsUIView.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 6/3/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import SwiftUI
import Firebase
import Combine

struct VillageStatisticsUIView: View {
    
    @ObservedObject var village = StatisticsViewController()
    @State var pickerSelectedItem = 0
    
    var body: some View {
        ZStack(alignment: .top) {
            Rectangle()
                .fill(Color("rectangleColor"))
                .cornerRadius(20)
            
            VStack(alignment: .leading) {
                Text("Villages")
                    .font(.system(size: 34))
                    .fontWeight(.heavy)
                    .padding([.top, .leading])
                
                VStack {
                    Picker(selection: $pickerSelectedItem, label: Text("")) {
                        Text("Public Schools").tag(0)
                        Text("Private Schools").tag(1)
                        Text("Population").tag(2)
                    }.pickerStyle(SegmentedPickerStyle())
                        .padding(.horizontal, 24)
                    
                    //Text("Picker: \(pickerSelectedItem)")
                    VillageView(village: village, pickerSelectedItem: pickerSelectedItem)
                }
            }
        }
    }
}

struct VillageView: View {
    @ObservedObject var village = StatisticsViewController()
    var pickerSelectedItem: Int
    var ref = Firestore.firestore().collection("villages").whereField("uid", isEqualTo: VillageManager.shared.uid)
    
    var body: some View {
        ZStack {
            VStack {
                HStack(spacing: 25) {
                    
                    if pickerSelectedItem == 0 {
                        ScrollView {
                            VStack {
                                if self.village.publicSchoolPieData == [0,0,0] {
                                    NoDataView()
                                } else {
                                    PieView(pieChartData: PieChartData(data: self.village.publicSchoolPieData), annotations: ["< 3", "3 - 6", "> 6"]).frame(width: 225, height: 225)
                                    ForEach(0 ..< village.villageNames.count, id: \.self) {
                                        HBarView(barName: self.village.villageNames[$0], value: self.village.publicSchools[self.village.villageNames[$0]] ?? 0)
                                    }
                                }
                                
                            }
                        }.onAppear(perform: {self.village.getVillageSchools(query: self.ref)})
                    } else if pickerSelectedItem == 1 {
                        ScrollView {
                            VStack {
                                if self.village.privateSchoolPieData == [0,0,0] {
                                    NoDataView()
                                } else {
                                    PieView(pieChartData: PieChartData(data: self.village.privateSchoolPieData), annotations: ["< 3", "3 - 6", "> 6"]).frame(width: 225, height: 225)
                                    ForEach(0 ..< village.villageNames.count, id: \.self) {
                                        HBarView(barName: self.village.villageNames[$0], value: self.village.privateSchools[self.village.villageNames[$0]] ?? 0)
                                    }
                                }
                                
                            }
                        }.onAppear(perform: {self.village.getVillageSchools(query: self.ref)})
                    } else if pickerSelectedItem == 2 {
                        ScrollView {
                            VStack {
                                if self.village.populationPieData == [0,0,0] {
                                    NoDataView()
                                } else {
                                    PieView(pieChartData: PieChartData(data: self.village.populationPieData), annotations: ["< 25", "25 - 50", "> 50"]).frame(width: 225, height: 225)
                                    ForEach(0 ..< village.villageNames.count, id: \.self) {
                                        HBarView(barName: self.village.villageNames[$0], value: self.village.populations[self.village.villageNames[$0]] ?? 0)
                                    }
                                }
                                
                            }
                        }.onAppear(perform: {self.village.getVillagePopulations(query: self.ref)})
                    }
                    
                    //BarView(barName: "Over 65", value: dataPoints[pickerSelectedItem][3])
                    
                }
                .animation(.default)
            }
        }.padding(.all, 24)
    }
}

struct VillageStatisticsUIView_Previews: PreviewProvider {
    static var previews: some View {
        VillageStatisticsUIView()
            .previewDevice(PreviewDevice(rawValue: "iPad Pro (12.9-inch)"))
    }
}

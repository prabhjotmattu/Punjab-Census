//
//  PersonStatisticsUIView.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 5/4/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import SwiftUI
import Firebase
import Combine

struct PersonStatisticsUIView: View {
    
    @ObservedObject var person1 = StatisticsViewController()
    @ObservedObject var person2 = StatisticsViewController()
    
    @State var pickerSelectedItem = 0
    
    var body: some View {
        //#colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
        
        ZStack(alignment: .top) {
            Rectangle()
                .fill(Color("rectangleColor"))
                .cornerRadius(20)
            
            VStack(alignment: .leading) {
                Text("People")
                    .font(.system(size: 34))
                    .fontWeight(.heavy)
                    .padding([.top, .leading])
                
                VStack {
                    Picker(selection: $pickerSelectedItem, label: Text("")) {
                        Text("Age").tag(0)
                        Text("Gender").tag(1)
                        Text("Education").tag(2)
                        Text("Religions").tag(3)
                        Text("Children").tag(4)
                        Text("Drug/Alchol Use").tag(5)
                    }.pickerStyle(SegmentedPickerStyle())
                        .padding(.horizontal, 24)
                    
                    //Text("Picker: \(pickerSelectedItem)")
                    PersonView(person: person1, pickerSelectedItem: pickerSelectedItem)
                    PersonView(person: person2, pickerSelectedItem: pickerSelectedItem)
                }
            }
        }
    }
}

struct PersonView: View {
    @ObservedObject var person = StatisticsViewController()
    var pickerSelectedItem: Int
    
    var body: some View {
        ZStack {
            VStack {
                
                
                HStack(spacing: 25) {
                    
                    if pickerSelectedItem == 0 {
                        if self.person.avgAge == 0 {
                            NoDataView()
                        } else {
                            BarView(barName: "Avg Age", value: self.person.avgAge)
                            BarView(barName: "Under 18", value: self.person.under18)
                            BarView(barName: "18-39", value: self.person.between18and39)
                            BarView(barName: "40-64", value: self.person.between40and64)
                            BarView(barName: "Over 65", value: self.person.over65)
                        }
                    } else if pickerSelectedItem == 1 {
                        if self.person.male == 0 && self.person.female == 0 {
                            NoDataView()
                        } else {
                            PieView(pieChartData: PieChartData(data: [self.person.male, self.person.female, 0]), annotations: ["Male", "Female", "N/A"]).frame(width: 225, height: 225)
                        }
                    } else if pickerSelectedItem == 2 {
                        if self.person.attending == 0 && self.person.highSchool == 0 && self.person.bachelors == 0 && self.person.masters == 0 {
                            NoDataView()
                        } else {
                            BarView(barName: "Attending", value: self.person.attending)
                            BarView(barName: "High School", value: self.person.highSchool)
                            BarView(barName: "Bachelors", value: self.person.bachelors)
                            BarView(barName: "Masters", value: self.person.masters)
                            BarView(barName: "N/A", value: self.person.na)
                        }
                        
                    } else if pickerSelectedItem == 3 {
                        if person.self.religionCounts == [] {
                            NoDataView()
                        } else {
                            PieView(pieChartData: PieChartData(data: person.self.religionCounts), annotations: self.person.religionNames).frame(width: 225, height: 225)
                        }
                        
                    } else if pickerSelectedItem == 4 {
                        if self.person.yesChildren == 0 && self.person.noChildren == 0 {
                            NoDataView()
                        } else {
                            PieView(pieChartData: PieChartData(data: [self.person.yesChildren, self.person.noChildren]), annotations: ["Yes", "No"]).frame(width: 225, height: 225)
                        }
                    } else if pickerSelectedItem == 5 {
                        if self.person.yesDrugs == 0 && self.person.noDrugs == 0 {
                            NoDataView()
                        } else {
                            PieView(pieChartData: PieChartData(data: [self.person.yesDrugs, self.person.noDrugs]), annotations: ["Yes", "No"]).frame(width: 225, height: 225)
                        }
                    }
                }
                .animation(.default)
                .onAppear(perform: {self.person.getPeopleQueries()})
                
                PopoverView(person: person, keys: self.person.villageKeys)
                    .padding(.top, 24)
            }
        }.padding(.all, 24)
    }
}

struct PopoverView: View {
    @ObservedObject var person = StatisticsViewController()
    @State private var showPopover: Bool = false
    @State private var selectedVillageKey = ""
    var keys: [String]
    @State var village = "All Villages"
    
    //let villages: [Villages] = [Villages(id: <#String#>, name: "DSfj"), Villages(name: "hsdflh")]
    
    
    var body: some View {
        VStack {
            
            Button(village) {
                self.showPopover = true
            }.popover(isPresented: self.$showPopover, arrowEdge: .bottom) {
                VStack(alignment: .trailing) {
                    Button(action: {
                        self.showPopover = false
                        self.village = self.person.villages[self.selectedVillageKey] ?? "All Villages"
                        self.person.getPeopleQueries(vid: self.selectedVillageKey)
                    }, label: {Text("Done")})
                        .padding(.trailing, 20)
                        .frame(alignment: .topTrailing)
                    
                    Picker(selection: self.$selectedVillageKey, label: Text("")) {
                        //Text("All Villages")
                        ForEach(self.keys, id: \.self) { key in
                            Text(self.person.villages[key] ?? "")
                        }
                    }.pickerStyle(WheelPickerStyle())
                }
                /*
                 List(self.keys, id: \.self) { key in
                 Text(self.person.villages[key] ?? "")
                 //Text(key)
                 
                 ForEach(self.person.villageKeys, id: \.self) { key in
                 
                 Text("hello")
                 //Text(key)
                 }
                 
                 }
                 */
            }
        }
    }
}

struct NoDataView: View {
    var body: some View {
        Text("No Data")
            .frame(width: 225, height: 225)
            .font(.system(size: 34))
    }
}

struct Villages: Identifiable {
    var id: String
    var name: String
}

struct PersonStatisticsUIView_Previews: PreviewProvider {
    static var previews: some View {
        PersonStatisticsUIView()
            .previewDevice(PreviewDevice(rawValue: "iPad Pro (12.9-inch)"))
    }
}

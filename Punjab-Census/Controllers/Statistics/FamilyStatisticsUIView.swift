//
//  FamilyStatisticsUIView.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 5/7/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import SwiftUI
import Firebase
import Combine

struct FamilyStatisticsUIView: View {
    
    @ObservedObject var family = StatisticsViewController()
    @State var pickerSelectedItem = 0
    
    
    var body: some View {
        ZStack(alignment: .top) {
            Rectangle()
                .fill(Color("rectangleColor"))
                .cornerRadius(20)
            
            VStack(alignment: .leading) {
                
                Text("Families")
                    .font(.system(size: 34))
                    .fontWeight(.heavy)
                    .padding([.top, .leading])
                
                
                VStack {
                    Picker(selection: $pickerSelectedItem, label: Text("")) {
                        Text("Family Size").tag(0)
                        Text("Income").tag(1)
                        Text("Transportation").tag(2)
                        Text("Tractor").tag(3)
                        Text("Overseas").tag(4)
                    }.pickerStyle(SegmentedPickerStyle())
                        .padding(.horizontal, 24)
                    
                    Picker(selection: $pickerSelectedItem, label: Text("")) {
                        Text("Police").tag(5)
                        Text("Internet").tag(6)
                        Text("Electricity").tag(7)
                        Text("Healthcare").tag(8)
                        Text("Water").tag(9)
                        Text("Food").tag(10)
                    }.pickerStyle(SegmentedPickerStyle())
                        .padding(.horizontal, 24)
                    
                    FamilyView(family: family, pickerSelectedItem: pickerSelectedItem)
                }
                
            }
        }.onAppear(perform: {self.family.getFamilyQueries()})
    }
}


struct FamilyView: View {
    @ObservedObject var family = StatisticsViewController()
    var pickerSelectedItem: Int
    var ref = Firestore.firestore().collection("families").whereField("uid", isEqualTo: FamilyManager.shared.uid)
    
    var body: some View {
        ZStack {
            VStack {
                HStack(spacing: 25) {
                    if pickerSelectedItem == 0 {
                        ScrollView {
                            VStack {
                                if self.family.familySizePieData == [0,0,0] {
                                    NoDataView()
                                } else {
                                    PieView(pieChartData: PieChartData(data: self.family.familySizePieData), annotations: ["< 3", "3 - 6", "> 6"]).frame(width: 225, height: 225)
                                    HBarView(barName: "Avg Family Size", value: family.avgFamilySize)
                                    ForEach(0 ..< family.familyNames.count, id: \.self) {
                                        HBarView(barName: self.family.familyNames[$0], value: self.family.familySizes[self.family.familyNames[$0]] ?? 0)
                                    }
                                }
                                
                            }
                        }.onAppear(perform: {self.family.getFamilySize(query: self.ref)})
                    } else if pickerSelectedItem == 1 {
                        ScrollView {
                            VStack {
                                if self.family.incomesPieData == [0,0,0] {
                                    NoDataView()
                                } else {
                                    PieView(pieChartData: PieChartData(data: self.family.incomesPieData), annotations: ["<10k", "10k-40k", ">40k"]).frame(width: 250, height: 250)
                                    HBarView(barName: "Avg Income", value: family.avgIncome)
                                    ForEach(0 ..< family.familyNames.count, id: \.self) {
                                        HBarView(barName: self.family.familyNames[$0], value: self.family.incomes[self.family.familyNames[$0]] ?? 0)
                                    }
                                }
                                
                            }
                        }.onAppear(perform: {self.family.getIncomeData(query: self.ref)})
                    } else if pickerSelectedItem == 2 {
                        ScrollView {
                            VStack {
                                if self.family.transportationPieData == [0,0,0] {
                                    NoDataView()
                                } else {
                                    PieView(pieChartData: PieChartData(data: self.family.transportationPieData), annotations: ["Walk", "Bus", "Car"]).frame(width: 250, height: 250).padding(.bottom, 24)
                                    HStack(spacing: 150) {
                                        listView(familyNames: family.transportationWalkFamilies, label: "Walk")
                                        listView(familyNames: family.transportationBusFamilies, label: "Bus")
                                        listView(familyNames: family.transportationCarFamilies, label: "Car")
                                    }
                                }
                            }
                        }
                        
                    } else if pickerSelectedItem == 3 {
                        ScrollView {
                            VStack {
                                if self.family.yesTractor == 0 && self.family.noTractor == 0 {
                                    NoDataView()
                                } else {
                                    PieView(pieChartData: PieChartData(data: [family.yesTractor, family.noTractor]), annotations: ["Yes", "No"]).frame(width: 250, height: 250).padding(.bottom, 24)
                                    HStack(spacing: 150) {
                                        listView(familyNames: family.yesTractorFamilies, label: "Yes")
                                        listView(familyNames: family.noTractorFamilies, label: "No")
                                    }
                                }
                                
                            }
                        }
                        
                    } else if pickerSelectedItem == 4 {
                        ScrollView {
                            VStack {
                                if self.family.yesOverseas == 0 && self.family.noOverseas == 0 {
                                    NoDataView()
                                } else {
                                    PieView(pieChartData: PieChartData(data: [family.yesOverseas, family.noOverseas]), annotations: ["Yes", "No"]).frame(width: 250, height: 250).padding(.bottom, 24)
                                    HStack(spacing: 150) {
                                        listView(familyNames: family.yesOverseasFamilies, label: "Yes")
                                        listView(familyNames: family.noOverseasFamilies, label: "No")
                                    }
                                }
                                
                            }
                        }
                        
                    } else if pickerSelectedItem == 5 {
                        ScrollView {
                            VStack {
                                if self.family.yesPolice == 0 && self.family.noPolice == 0 {
                                    NoDataView()
                                } else {
                                    PieView(pieChartData: PieChartData(data: [family.yesPolice, family.noPolice]), annotations: ["Yes", "No"]).frame(width: 250, height: 250).padding(.bottom, 24)
                                    HStack(spacing: 150) {
                                        listView(familyNames: family.yesPoliceFamilies, label: "Yes")
                                        listView(familyNames: family.noPoliceFamilies, label: "No")
                                    }
                                }
                                
                            }
                        }
                        
                    } else if pickerSelectedItem == 6 {
                        ScrollView {
                            VStack {
                                if self.family.yesInternet == 0 && self.family.noInternet == 0 {
                                    NoDataView()
                                } else {
                                    PieView(pieChartData: PieChartData(data: [family.yesInternet, family.noInternet]), annotations: ["Yes", "No"]).frame(width: 250, height: 250).padding(.bottom, 24)
                                    HStack(spacing: 150) {
                                        listView(familyNames: family.yesInternetFamilies, label: "Yes")
                                        listView(familyNames: family.noInternetFamilies, label: "No")
                                    }
                                }
                                
                            }
                        }
                    } else if pickerSelectedItem == 7 {
                        ScrollView {
                            VStack {
                                if self.family.yesElectricity == 0 && self.family.noElectricity == 0 {
                                    NoDataView()
                                } else {
                                    PieView(pieChartData: PieChartData(data: [family.yesElectricity, family.noElectricity]), annotations: ["Yes", "No"]).frame(width: 250, height: 250).padding(.bottom, 24)
                                    HStack(spacing: 150) {
                                        listView(familyNames: family.yesElectricityFamilies, label: "Yes")
                                        listView(familyNames: family.noElectricityFamilies, label: "No")
                                    }
                                }
                                
                            }
                        }
                        
                    } else if pickerSelectedItem == 8 {
                        ScrollView {
                            VStack {
                                if self.family.yesHealthcare == 0 && self.family.noHealthcare == 0 {
                                    NoDataView()
                                } else {
                                    PieView(pieChartData: PieChartData(data: [family.yesHealthcare, family.noHealthcare]), annotations: ["Yes", "No"]).frame(width: 250, height: 250).padding(.bottom, 24)
                                    HStack(spacing: 150) {
                                        listView(familyNames: family.yesHealthcareFamilies, label: "Yes")
                                        listView(familyNames: family.noHealthcareFamilies, label: "No")
                                    }
                                }
                                
                            }
                        }
                        
                    } else if pickerSelectedItem == 9 {
                        ScrollView {
                            VStack {
                                if self.family.yesWater == 0 && self.family.noWater == 0 {
                                    NoDataView()
                                } else {
                                    PieView(pieChartData: PieChartData(data: [family.yesWater, family.noWater]), annotations: ["Yes", "No"]).frame(width: 250, height: 250).padding(.bottom, 24)
                                    HStack(spacing: 150) {
                                        listView(familyNames: family.yesWaterFamilies, label: "Yes")
                                        listView(familyNames: family.noWaterFamilies, label: "No")
                                    }
                                }
                                
                            }
                        }
                        
                    } else if pickerSelectedItem == 10 {
                        ScrollView {
                            VStack {
                                if self.family.yesFood == 0 && self.family.noFood == 0 {
                                    NoDataView()
                                } else {
                                    PieView(pieChartData: PieChartData(data: [family.yesFood, family.noFood]), annotations: ["Yes", "No"]).frame(width: 250, height: 250).padding(.bottom, 24)
                                    HStack(spacing: 150) {
                                        listView(familyNames: family.yesFoodFamilies, label: "Yes")
                                        listView(familyNames: family.noFoodFamilies, label: "No")
                                    }
                                }
                            }
                        }
                        
                    }
                }.padding(.top, 24)
                    .animation(.default)
                
            }
        }
    }
}

struct listView: View {
    var familyNames: [String]
    var label: String
    
    var body: some View {
        ScrollView {
            VStack {
                Text(label)
                    .font(.system(size: 24))
                    .fontWeight(.heavy)
                //list(familyNames: familyNames)
                
                ForEach(0 ..< familyNames.count, id: \.self) {
                    Text(self.familyNames[$0])
                        .font(.system(size: 18))
                        .fontWeight(.light)
                }
                
            }
        }
    }
}

struct list: View {
    var familyNames: [String]
    
    var body: some View {
        VStack {
            ForEach(0 ..< familyNames.count, id: \.self) {
                Text(self.familyNames[$0])
                    .font(.system(size: 18))
                    .fontWeight(.light)
            }
        }
    }
}



struct FamilyStatisticsUIView_Previews: PreviewProvider {
    static var previews: some View {
        FamilyStatisticsUIView()
            .previewDevice(PreviewDevice(rawValue: "iPad Pro (12.9-inch)"))
    }
}

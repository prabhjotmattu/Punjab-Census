//
//  Person.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 3/25/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase
import Foundation

struct Person: Equatable {
    let uid: String
    let pid: String
    let vid: String
    let fid: String
    let firstName: String
    let lastName: String
    let age: Int
    let gender: String
    let religion: String
    let children: Bool
    let drug: Bool
    
    let school: String
    let education: String
    let profession: String
    
    let comment: String
}

//
//  Family.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 3/24/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase
import Foundation

struct Family {
    let uid: String
    let fid: String
    let familyName: String
    let income: Double
    let transportation: String
    let tractor: Bool
    let overseas: Bool
    let police: Bool
    
    let internet: Bool
    let electricity: Bool
    let healthcare: Bool
    let water: Bool
    let food: Bool
    
    let comment: String
    
    var members: [String]
}

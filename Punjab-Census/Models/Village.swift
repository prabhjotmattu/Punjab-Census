//
//  Village.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 3/24/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase
import Foundation

struct Village {
    let uid: String
    let vid: String
    let villageName: String
    let numOfResident: Int
    let numOfPubSchool: Int
    let numOfPrivSchool: Int
    
    let comment: String
}

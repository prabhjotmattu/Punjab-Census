//
//  AppDelegate.swift
//  Punjab-Census
//
//  Created by Prabhjot Mattu on 3/21/20.
//  Copyright © 2020 RadioASP. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //Switcher.updateRootVC()
        FirebaseApp.configure()
        
        /*
        let splitViewController = UISplitViewController()
        
        let peopleMasterViewController = PeopleMasterViewController()
        let primaryViewController = UINavigationController(rootViewController: peopleMasterViewController)
        
        let peopleDetailViewController = PeopleDetailViewController()
        let secondaryViewController = UINavigationController(rootViewController: peopleDetailViewController)
        
        peopleMasterViewController.delegate = peopleDetailViewController
        */
        
        
        /*
        // Style window
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController()
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
        */
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    
}

